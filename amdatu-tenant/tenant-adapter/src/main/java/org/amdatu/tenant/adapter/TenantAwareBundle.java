/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Map;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;

/**
 * Wrapper class for the bundle interface to make it tenant aware.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantAwareBundle implements Bundle {

    private final TenantAwareBundleContext m_scopedBundleContext;
    private final Bundle m_parentBundle;

    public TenantAwareBundle(TenantAwareBundleContext context, Bundle parentBundle) {
        m_scopedBundleContext = context;
        m_parentBundle = parentBundle;
    }

    public TenantAwareBundle(TenantAwareBundleContext context) {
        this(context, context.getParent().getBundle());
    }

    public TenantAwareBundle(Bundle bundle) {
        this(null, bundle);
    }

    /*
     * Public access
     */

    public Bundle getParent() {
        return m_parentBundle;
    }

    /*
     * Bundle interface
     */

    /**
     * @see org.osgi.framework.Bundle#getState()
     */
    public int getState() {
        return getParent().getState();
    }

    /**
     * @see org.osgi.framework.Bundle#start(int)
     */
    public void start(int options) throws BundleException {
        getParent().start(options);
    }

    /**
     * @see org.osgi.framework.Bundle#start()
     */
    public void start() throws BundleException {
        getParent().start();
    }

    /**
     * @see org.osgi.framework.Bundle#stop(int)
     */
    public void stop(int options) throws BundleException {
        getParent().stop(options);
    }

    /**
     * @see org.osgi.framework.Bundle#stop()
     */
    public void stop() throws BundleException {
        getParent().stop();
    }

    /**
     * @see org.osgi.framework.Bundle#update(java.io.InputStream)
     */
    public void update(InputStream input) throws BundleException {
        getParent().update(input);
    }

    /**
     * @see org.osgi.framework.Bundle#update()
     */
    public void update() throws BundleException {
        getParent().update();
    }

    /**
     * @see org.osgi.framework.Bundle#uninstall()
     */
    public void uninstall() throws BundleException {
        getParent().uninstall();
    }

    /**
     * @see org.osgi.framework.Bundle#getHeaders()
     */
    public Dictionary getHeaders() {
        return getParent().getHeaders();
    }

    /**
     * @see org.osgi.framework.Bundle#getBundleId()
     */
    public long getBundleId() {
        return getParent().getBundleId();
    }

    /**
     * @see org.osgi.framework.Bundle#getLocation()
     */
    public String getLocation() {
        return getParent().getLocation();
    }

    /**
     * @see org.osgi.framework.Bundle#getRegisteredServices()
     */
    public ServiceReference[] getRegisteredServices() {
        return getParent().getRegisteredServices();
    }

    /**
     * @see org.osgi.framework.Bundle#getServicesInUse()
     */
    public ServiceReference[] getServicesInUse() {
        return getParent().getServicesInUse();
    }

    /**
     * @see org.osgi.framework.Bundle#hasPermission(java.lang.Object)
     */
    public boolean hasPermission(Object permission) {
        return getParent().hasPermission(permission);
    }

    /**
     * @see org.osgi.framework.Bundle#getResource(java.lang.String)
     */
    public URL getResource(String name) {
        return getParent().getResource(name);
    }

    /**
     * @see org.osgi.framework.Bundle#getHeaders(java.lang.String)
     */
    public Dictionary getHeaders(String locale) {
        return getParent().getHeaders(locale);
    }

    /**
     * @see org.osgi.framework.Bundle#getSymbolicName()
     */
    public String getSymbolicName() {
        return getParent().getSymbolicName();
    }

    /**
     * @see org.osgi.framework.Bundle#loadClass(java.lang.String)
     */
    public Class loadClass(String name) throws ClassNotFoundException {
        return getParent().loadClass(name);
    }

    /**
     * @see org.osgi.framework.Bundle#getResources(java.lang.String)
     */
    public Enumeration getResources(String name) throws IOException {
        return getParent().getResources(name);
    }

    /**
     * @see org.osgi.framework.Bundle#getEntryPaths(java.lang.String)
     */
    public Enumeration getEntryPaths(String path) {
        return getParent().getEntryPaths(path);
    }

    /**
     * @see org.osgi.framework.Bundle#getEntry(java.lang.String)
     */
    public URL getEntry(String path) {
        return getParent().getEntry(path);
    }

    /**
     * @see org.osgi.framework.Bundle#getLastModified()
     */
    public long getLastModified() {
        return getParent().getLastModified();
    }

    /**
     * @see org.osgi.framework.Bundle#findEntries(java.lang.String, java.lang.String, boolean)
     */
    public Enumeration findEntries(String path, String filePattern, boolean recurse) {
        return getParent().findEntries(path, filePattern, recurse);
    }

    /**
     * @see org.osgi.framework.Bundle#getBundleContext()
     */
    public BundleContext getBundleContext() {
        return m_scopedBundleContext;
    }

    /**
     * @see org.osgi.framework.Bundle#getSignerCertificates(int)
     */
    public Map getSignerCertificates(int signersType) {
        return getParent().getSignerCertificates(signersType);
    }

    /**
     * @see org.osgi.framework.Bundle#getVersion()
     */
    public Version getVersion() {
        return getParent().getVersion();
    }
}
