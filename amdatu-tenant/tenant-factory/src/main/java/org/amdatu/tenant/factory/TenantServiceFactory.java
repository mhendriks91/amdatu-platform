/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.factory;

import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_BOTH;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_KEY;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS;
import static org.amdatu.tenant.Constants.NAME_KEY;
import static org.amdatu.tenant.Constants.NAME_VALUE_PLATFORM;
import static org.amdatu.tenant.Constants.PID_KEY;
import static org.amdatu.tenant.Constants.PID_VALUE_PLATFORM;

import java.util.Arrays;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.amdatu.tenant.Constants;
import org.amdatu.tenant.Tenant;
import org.amdatu.tenant.TenantLifeCycleListener;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

/**
 * Amdatu core {@link ManagedServiceFactory} implementation responsible for
 * publishing {@link Tenant} services based on configuration provided under
 * factoryPid {@link TenantServiceFactory.PID}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public final class TenantServiceFactory implements ManagedServiceFactory {

    public static final String PID = "org.amdatu.tenant.factory";

    private final Object m_lock = new Object();
    private final Map<String, Component> m_components = new HashMap<String, Component>();
    private final Map<ServiceReference, TenantLifeCycleListener> m_listeners =
        new HashMap<ServiceReference, TenantLifeCycleListener>();
    private final Map<ServiceReference, TenantLifeCycleListener> m_platformListeners =
        new HashMap<ServiceReference, TenantLifeCycleListener>();

    private volatile DependencyManager m_dependencyManager;
    private volatile LogService m_logService;

    public void add(ServiceReference reference, TenantLifeCycleListener listener) {
        String[] pids;
        Integer bindingBits = (Integer) reference.getProperty(MULTITENANT_LIFECYCLELISTENER_BINDING_KEY);
        int binding = bindingBits != null ? bindingBits.intValue() : MULTITENANT_LIFECYCLELISTENER_BINDING_BOTH;
        synchronized (m_lock) {
            boolean isPlatformBinding = (binding & MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM) > 0;
            if (isPlatformBinding) {
                m_platformListeners.put(reference, listener);
            }
            boolean isTenantBinding = (binding & MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS) > 0;
            if (isTenantBinding) {
                m_listeners.put(reference, listener);
            }
            pids = new String[m_components.size()];
            Iterator<Component> iterator = m_components.values().iterator();
            int i = 0;
            while (i < pids.length && iterator.hasNext()) {
                String pid = (String) iterator.next().getServiceProperties().get(Constants.PID_KEY);
                boolean isPlatformPid = pid.equals(Constants.PID_VALUE_PLATFORM);
                if ((isPlatformBinding && isPlatformPid) || (isTenantBinding && !isPlatformPid)) {
                    pids[i++] = pid;
                }
            }
            // because of the binding, we might have ended up with an array of a smaller size
            // in practice: N, N-1, 1 or 0
            if (i < pids.length) {
                pids = Arrays.copyOf(pids, i);
            }
        }
        listener.initial(pids);
    }

    public void remove(ServiceReference reference, TenantLifeCycleListener listener) {
        synchronized (m_lock) {
            // instead of trying to figure out the binding, we simply try to remove the listener
            // from both lists
            m_platformListeners.remove(reference);
            m_listeners.remove(reference);
        }
    }

    /**
     * @see org.osgi.service.cm.ManagedServiceFactory#deleted(java.lang.String)
     */
    public void deleted(String pid) {
        Component component;
        TenantLifeCycleListener[] listeners;
        String tenantPid;
        synchronized (m_lock) {
            component = m_components.remove(pid);
            tenantPid = (String) component.getServiceProperties().get(Constants.PID_KEY);
            Collection<TenantLifeCycleListener> values = m_listeners.values();
            listeners = values.toArray(new TenantLifeCycleListener[values.size()]);
        }
        if (component != null) {
            m_logService.log(LogService.LOG_INFO, "Unregistering tenant service with pid: '" + pid + "'");
            m_dependencyManager.remove(component);
        }
        for (TenantLifeCycleListener listener : listeners) {
            try {
                listener.delete(tenantPid);
            }
            catch (Exception e) {
                m_logService.log(LogService.LOG_WARNING,
                    "Tenant life cycle listener returned with an exception when invoking delete for PID: " + pid, e);
            }
        }
    }

    /**
     * @see org.osgi.service.cm.ManagedServiceFactory#getName()
     */
    public String getName() {
        return PID;
    }

    /**
     * Called by Felix dependency manager upon start of this component.
     */
    public void start() throws Exception {
        Properties props = new Properties();
        props.put(PID_KEY, PID_VALUE_PLATFORM);
        props.put(NAME_KEY, NAME_VALUE_PLATFORM);
        addTenant(PID_VALUE_PLATFORM, props);
    }

    /**
     * Called by Felix dependency manager upon stop of this component.
     */
    public void stop() {
    }

    /**
     * @see org.osgi.service.cm.ManagedServiceFactory#updated(java.lang.String, java.util.Dictionary)
     */
    public void updated(String pid, Dictionary/* <String, Object> */properties)
        throws ConfigurationException {

        Dictionary serviceProperties = copyProperties(properties);

        Component component = null;
        synchronized (m_lock) {
            component = m_components.get(pid);
        }
        if (component != null) {
            updateTenant(component, serviceProperties);
        }
        else {
            addTenant(pid, serviceProperties);
        }
    }

    private void addTenant(String pid, Dictionary serviceProperties) throws ConfigurationException {
        Component component;

        String tenantPid = (String) serviceProperties.get(Constants.PID_KEY);

        // Create a new tenant...
        component = m_dependencyManager.createComponent()
            .setInterface(Tenant.class.getName(), serviceProperties)
            .setImplementation(new TenantService(serviceProperties));

        TenantLifeCycleListener[] listeners;
        synchronized (m_lock) {
            m_components.put(pid, component);
            Collection<TenantLifeCycleListener> values;
            if (PID_VALUE_PLATFORM.equals(tenantPid)) {
                values = m_platformListeners.values();
            }
            else {
                values = m_listeners.values();
            }
            listeners = values.toArray(new TenantLifeCycleListener[values.size()]);
        }
        for (TenantLifeCycleListener listener : listeners) {
            try {
                listener.create(tenantPid);
            }
            catch (Exception e) {
                m_logService.log(LogService.LOG_WARNING,
                    "Tenant life cycle listener returned with an exception when invoking create for PID: " + pid, e);
                e.printStackTrace();
            }
        }

        m_logService.log(LogService.LOG_DEBUG,
            "Registering tenant service with pid: '" + serviceProperties.get(PID_KEY) + "'");
        m_dependencyManager.add(component);
    }

    /**
     * Creates a shallow copy of a given dictionary.
     * 
     * @param dictionary the dictionary to copy, can be <code>null</code>.
     * @return a copy of the given dictionary, can only be <code>null</code> if the given dictionary was
     *         <code>null</code>.
     */
    private Dictionary copyProperties(Dictionary dictionary) {
        Properties copy = null;
        if (dictionary != null) {
            copy = new Properties();

            Enumeration<String> keys = dictionary.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                copy.put(key, dictionary.get(key));
            }
        }
        return copy;
    }

    private void updateTenant(Component component, Dictionary serviceProperties) throws ConfigurationException {
        Object givenPID = serviceProperties.get(PID_KEY);
        Object currentPID = component.getServiceProperties().get(PID_KEY);

        // Tenant is already registered; lets update its configuration...
        // Verify that the tenant PID is not overwritten!
        if (givenPID != null && !currentPID.equals(givenPID)) {
            throw new ConfigurationException(PID_KEY, "Attempt to overwrite " + PID_KEY + " property from "
                + currentPID + " to " + givenPID);
        }

        m_logService.log(LogService.LOG_DEBUG, "Updating tenant service with pid: '" + currentPID + "'");

        component.setServiceProperties(serviceProperties);
    }
}
