/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.factory;

import org.amdatu.tenant.factory.TenantService;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test cases for {@link TenantService}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantServiceTest {

    /**
     * Tests that the constructor where a tenant-id is given does not accept null as tenant-id.
     */
    @Test(expected = NullPointerException.class)
    public void testTenantIdIsMandatory() {
        new TenantService(null, "foo");
    }

    /**
     * Tests that the constructor with service properties does not accept a null-value.
     */
    @Test(expected = NullPointerException.class)
    public void testTenantServicePropertiesIsMandatory() {
        new TenantService(null);
    }

    /**
     * Tests that the {@link Object#equals(Object)} method for {@link TenantService} only takes the "tenant.id" into consideration.
     * See also AMDATU-260.
     */
    @Test
    public void testEquality() {
        TenantService t1 = new TenantService("t13245", "Kwik");
        TenantService t2 = new TenantService("t13245", "Kwek");
        Assert.assertEquals(t1, t2);

        TenantService t3 = new TenantService("t23245", "Kwek");
        Assert.assertNotSame(t2, t3);
    }
}
