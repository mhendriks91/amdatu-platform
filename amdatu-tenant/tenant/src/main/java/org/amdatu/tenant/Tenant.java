/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant;

/**
 * This interface represents a tenant. A tenant has an PID that uniquely identifies it and an optional name.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface Tenant {

    /**
     * The mandatory unique PID for this tenant.
     * 
     * @return a persistent identifier, never <code>null</code>.
     */
    String getPID();

    /**
     * The optional name for this tenant.
     * 
     * @return a name describing the tenant, can be <code>null</code>.
     */
    String getName();
}
