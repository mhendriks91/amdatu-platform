/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.List;

import org.amdatu.ace.client.AceClient;
import org.amdatu.ace.client.AceClientException;

/**
 * Container representing a client side view of a workspace used for imports and exports.
 */
public class Model {

    // @checkstyle:ignore
    private ResourceMap resources = new ResourceMap();

    public Model() {
        for (Class<? extends AbstractResource> c : AceClient.RESOURCES_CLASSES) {
            String path = c.getAnnotation(Resource.class).path();
            resources.put(path, new LinkedList<AbstractResource>());
        }
    }

    public <TYPE extends AbstractResource> void addResources(TYPE[] res) throws AceClientException {
        for (TYPE resource : res) {
            addResource(resource);
        }
    }

    public <TYPE extends AbstractResource> void addResource(TYPE res) throws AceClientException {
        if (!AceClient.RESOURCES_CLASSES.contains(res.getClass())) {
            throw new AceClientException("Unknown resource class");
        }
        String path = res.getClass().getAnnotation(Resource.class).path();
        resources.get(path).add(res);
    }

    public <TYPE extends AbstractResource> void removeResources(TYPE[] res) throws AceClientException {
        for (TYPE resource : res) {
            addResource(resource);
        }
    }

    public <TYPE extends AbstractResource> void removeResource(TYPE res) throws AceClientException {
        if (!AceClient.RESOURCES_CLASSES.contains(res.getClass())) {
            throw new AceClientException("Unknown resource class");
        }
        String path = res.getClass().getAnnotation(Resource.class).path();
        resources.get(path).remove(res);
    }

    public <TYPE extends AbstractResource> TYPE[] getResources(Class<TYPE> resClass) throws AceClientException {
        if (!AceClient.RESOURCES_CLASSES.contains(resClass)) {
            throw new AceClientException("Unknown resource class:" + resClass.getName());
        }
        String path = resClass.getAnnotation(Resource.class).path();
        List<AbstractResource> l = resources.get(path);
        /* JavaNCSS crashes on this: @SuppressWarnings("unchecked") */
        TYPE[] result = (TYPE[]) Array.newInstance(resClass, l.size());
        return l.toArray(result);
    }
}
