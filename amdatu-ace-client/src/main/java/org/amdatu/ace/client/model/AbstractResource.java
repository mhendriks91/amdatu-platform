/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides an abstract base class for all resources retrieved from Ace.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public abstract class AbstractResource {

    // @checkstyle:off
    private String definition;
    private final Map<String, String> attributes = new HashMap<String, String>();
    private final Map<String, String> tags = new HashMap<String, String>();

    // @checkstyle:on

    /**
     * Returns the definition, or resource-identifier, of this resource.
     * 
     * @return the definition of this resource, that uniquely identifies this resource in Ace, can be <code>null</code>
     *         in case the resource hasn't been committed to Ace yet.
     */
    public final String getDefinition() {
        return definition;
    }

    /**
     * Returns the attribute with the given name from this resource.
     * 
     * @param key the name of the attribute to retrieve.
     * @return the attribute value, can be <code>null</code>.
     */
    public String getAttribute(String key) {
        return attributes.get(key);
    }

    /**
     * Sets the attribute with the given name to a given value.
     * 
     * @param key the name of the attribute to set, cannot be <code>null</code>;
     * @param value the value of the attribute to set, may be <code>null</code>.
     */
    public void setAttribute(String key, String value) {
        attributes.put(key, value);
    }

    /**
     * Returns all attributes of this resource.
     * 
     * @return a map of all attributes, never <code>null</code>.
     */
    public Map<String, String> getAttributes() {
        return attributes;
    }

    /**
     * Sets all attributes of this resource.
     * 
     * @param allAttributes the attributes to set, can be <code>null</code>.
     */
    public void setAttributes(Map<String, String> allAttributes) {
        attributes.clear();
        if (allAttributes != null) {
            for (String key : allAttributes.keySet()) {
                attributes.put(key, allAttributes.get(key));
            }
        }
    }

    /**
     * Returns the tag with the given name.
     * 
     * @param key the name of the tag to retrieve.
     * @return the value of the tag, can be <code>null</code>.
     */
    public String getTag(String key) {
        return tags.get(key);
    }

    /**
     * Sets the tag with the given name to a given value.
     * 
     * @param key the name of the tag to set, cannot be <code>null</code>;
     * @param value the value of the tag to set, may be <code>null</code>.
     */
    public void setTag(String key, String value) {
        tags.put(key, value);
    }

    /**
     * Returns all tags of this resource.
     * 
     * @return a map with all tags, never <code>null</code>.
     */
    public Map<String, String> getTags() {
        return tags;
    }

    /**
     * Sets all tags of this resource.
     * 
     * @param allTags the tags to set, can be <code>null</code>.
     */
    public void setTags(Map<String, String> allTags) {
        tags.clear();
        if (allTags != null) {
            for (String key : allTags.keySet()) {
                tags.put(key, allTags.get(key));
            }
        }
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !(obj instanceof AbstractResource)) {
            return false;
        }

        AbstractResource other = (AbstractResource) obj;
        if (!other.getAttributes().equals(attributes)) {
            return false;
        }
        if (!other.getTags().equals(tags)) {
            return false;
        }

        return true;
    }
}