/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;


/**
 * Denotes a target in Ace.
 * <p>
 * A target is the only concept in Ace that has additional state stored. This
 * state is represented by {@link TargetState} which is already returned with
 * this {@link Target}, making it unnecessary to explicitly retrieve this
 * state. Hence the composite relation between {@link Target} and {@link TargetState}.
 * </p>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@Resource(path = "target")
public class Target extends AbstractResource {

    public static final String ATTR_ID = "id";
    public static final String ATTR_AUTOAPPROVE = "autoapprove";

    // @checkstyle:ignore
    private TargetState state = new TargetState();

    /**
     * Returns the identifier of this target.
     * 
     * @return the identifier, never <code>null</code>.
     */
    public String getId() {
        return getAttribute(ATTR_ID);
    }

    /**
     * Returns the state of this target.
     * 
     * @return the state of this target, cannot be <code>null</code>.
     */
    public TargetState getState() {
        return state;
    }

    /**
     * Convenience method to determine whether or not this target auto approves new updates.
     * 
     * @return <code>true</code> if this target auto approves new updates, <code>false</code> otherwise.
     */
    public boolean isAutoApprove() {
        return getAttribute(ATTR_AUTOAPPROVE) != null && getAttribute(ATTR_AUTOAPPROVE).equals("true");
    }

    /**
     * Convenience method to set auto approves on an existing target.
     * 
     * @param autoapprove
     */
    public void setAutoApprove(boolean autoapprove) {
        setAttribute(ATTR_AUTOAPPROVE, autoapprove ? "true" : "false");
    }
}
