/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import org.amdatu.ace.client.AceClientException;

/**
 * Provides an abstract base class for resource builders.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 * @param <BUILDER> the type of builder to use in this builder;
 * @param <TYPE> the resource to build in this builder.
 */
public abstract class AbstractBuilder<BUILDER extends AbstractBuilder<BUILDER, TYPE>, TYPE extends AbstractResource> {

    /** The resource instance to build. */
    protected final TYPE m_instance;

    /**
     * Creates a new {@link AbstractBuilder} instance.
     * 
     * @param instance the resource to build, cannot be <code>null</code>.
     */
    protected AbstractBuilder(TYPE instance) {
        if (instance == null) {
            throw new IllegalArgumentException("Resource cannot be null!");
        }
        m_instance = instance;
    }

    /**
     * Builds the resource.
     * 
     * @return the resource to build, never <code>null</code>.
     * @throws AceClientException in case the builder could not fully build the resource.
     */
    public abstract TYPE build() throws AceClientException;

    /**
     * Sets the attribute with a given name to a given value.
     * 
     * @param key the name of the attribute to set, cannot be <code>null</code> or empty;
     * @param value the value of the attribute to set, may be <code>null</code> or empty.
     * @return this builder.
     */
    public final BUILDER setAttribute(String key, String value) {
        if (key == null || key.trim().equals("")) {
            throw new IllegalArgumentException("Attribute key cannot be null or empty!");
        }
        m_instance.setAttribute(key, value);
        return getThis();
    }

    /**
     * Sets the tag with a given name to a given value.
     * 
     * @param key the name of the tag to set, cannot be <code>null</code> or empty;
     * @param value the value of the tag to set, may be <code>null</code> or empty.
     * @return this builder.
     */
    public final BUILDER setTag(String key, String value) {
        if (key == null || key.trim().equals("")) {
            throw new IllegalArgumentException("Attribute key cannot be null or empty!");
        }
        m_instance.setTag(key, value);
        return getThis();
    }

    /**
     * Returns the value of the attribute with a given name.
     * 
     * @param key the name of the attribute to return, cannot be <code>null</code> or empty.
     * @return the attribute's value, can be <code>null</code> or empty.
     */
    protected final String getAttribute(String key) {
        if (key == null || key.trim().equals("")) {
            throw new IllegalArgumentException("Attribute key cannot be null or empty!");
        }
        return m_instance.getAttribute(key);
    }

    /**
     * Returns the value of the tag with a given name.
     * 
     * @param key the name of the tag to return, cannot be <code>null</code> or empty.
     * @return the tag's value, can be <code>null</code> or empty.
     */
    protected final String getTag(String key) {
        if (key == null || key.trim().equals("")) {
            throw new IllegalArgumentException("Attribute key cannot be null or empty!");
        }
        return m_instance.getTag(key);
    }

    /**
     * Returns this builder.
     * 
     * @return this builder, never <code>null</code>.
     */
    protected abstract BUILDER getThis();
}
