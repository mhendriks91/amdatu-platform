/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Denotes an audit event for a target.
 * <p>
 * Audit events are read-only representations of events occurring at a target,
 * such as framework changes, installations of deployment packages and so on.
 * </p>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AuditEvent {

    // @checkstyle:off
    private String logId;
    private String id;
    private Date time;
    private String type;
    private Map<String, String> properties;

    // @checkstyle:on

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final AuditEvent other = (AuditEvent) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        }
        else if (!id.equals(other.id)) {
            return false;
        }

        if (logId == null) {
            if (other.logId != null) {
                return false;
            }
        }
        else if (!logId.equals(other.logId)) {
            return false;
        }

        if (properties == null) {
            if (other.properties != null) {
                return false;
            }
        }
        else if (!properties.equals(other.properties)) {
            return false;
        }

        if (time == null) {
            if (other.time != null) {
                return false;
            }
        }
        else if (!time.equals(other.time)) {
            return false;
        }

        if (type != other.type) {
            return false;
        }

        return true;
    }

    /**
     * Returns the identifier of this event.
     * 
     * @return the event identifier, never <code>null</code>.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the log identifier.
     * 
     * @return the log identifier, never <code>null</code>.
     */
    public String getLogId() {
        return logId;
    }

    /**
     * Returns the value of a custom property.
     * 
     * @param key the name of the property to get, cannot be <code>null</code> or empty.
     * @return the value of the property, can be <code>null</code>.
     * @throws IllegalArgumentException in case the given key was <code>null</code> or empty.
     */
    public String getProperty(final String key) {
        if (key == null || key.trim().equals("")) {
            throw new IllegalArgumentException("Key cannot be null or empty!");
        }
        return properties.get(key);
    }

    /**
     * Returns all names of custom properties.
     * 
     * @return an array with names of the defined properties, can be empty but never <code>null</code>.
     */
    public String[] getPropertyNames() {
        Set<String> keySet = properties.keySet();
        return keySet.toArray(new String[keySet.size()]);
    }

    /**
     * Returns the timestamp of this event.
     * 
     * @return a timestamp, never <code>null</code>.
     */
    public Date getTime() {
        return time;
    }

    /**
     * Returns the event type.
     * 
     * @return the type as string, never <code>null</code>.
     */
    public String getType() {
        return type;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((logId == null) ? 0 : logId.hashCode());
        result = prime * result + ((properties == null) ? 0 : properties.hashCode());
        result = prime * result + ((time == null) ? 0 : time.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    /**
     * Returns whether there are custom properties available.
     * 
     * @return <code>true</code> if there are custom properties for this audit event, <code>false</code> otherwise.
     */
    public boolean hasProperties() {
        return !properties.isEmpty();
    }
}
