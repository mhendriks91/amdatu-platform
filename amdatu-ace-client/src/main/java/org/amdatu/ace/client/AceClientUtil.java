/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.amdatu.ace.client.model.AbstractResource;
import org.amdatu.ace.client.model.Resource;
import org.amdatu.ace.client.model.ResourceMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public final class AceClientUtil {

    private AceClientUtil() {
    }

    private static final Gson GSON = new GsonBuilder()
        .disableHtmlEscaping()
        .setPrettyPrinting()
        .registerTypeAdapter(ResourceMap.class, new ResourceMapDeserializer())
        .create();

    public static String toJson(Object object) {
        return GSON.toJson(object);
    }

    public static <T extends Object> T fromJson(String data, Class<T> clazz) {
        return GSON.fromJson(data, clazz);
    }

    public static String encodeId(String id) {
        try {
            return URLEncoder.encode(id, "UTF-8").replaceAll("\\+", "%20");
        }
        catch (UnsupportedEncodingException e) {
        }
        return null;
    }

    public static String decodeId(String id) {
        try {
            return URLDecoder.decode(id, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
        }
        return null;
    }

    static class ResourceMapDeserializer implements JsonDeserializer<Map<String, List<AbstractResource>>> {

        public ResourceMap deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {

            JsonObject object = json.getAsJsonObject();
            ResourceMap map = new ResourceMap();
            for (Class<? extends AbstractResource> c : AceClient.RESOURCES_CLASSES) {

                String path = c.getAnnotation(Resource.class).path();
                JsonElement je = object.get(path);
                List<AbstractResource> entries = new LinkedList<AbstractResource>();

                if (je != null) {
                    JsonArray ja = je.getAsJsonArray();
                    for (int i = 0; i < ja.size(); i++) {
                        entries.add((AbstractResource) context.deserialize(ja.get(i), c));
                    }
                }
                map.put(path, entries);
            }
            return map;
        }
    }
}
