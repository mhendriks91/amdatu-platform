/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.ace.client.model.AbstractResource;
import org.amdatu.ace.client.model.Artifact;
import org.amdatu.ace.client.model.ArtifactBuilder;
import org.amdatu.ace.client.model.Model;
import org.amdatu.ace.client.test.SimpleHttpServer;
import org.amdatu.ace.client.test.ValidateResourceCollectionGet;
import org.amdatu.ace.client.test.ValidateResourceCreate;
import org.amdatu.ace.client.test.ValidateResourceDelete;
import org.amdatu.ace.client.test.ValidateWorkspaceCreate;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;
import org.mortbay.jetty.Handler;

/**
 * Tests for the AceClientWorkspace using a mock Jetty backend
 * that allows assertions on interaction at the HTTP level.
 */
public class AceClientWorkspaceTest {

    public final static String SCHEME = "http";
    public final static String HOST = "localhost";
    public final static String CLIENT_PATH = "/client/work";
    public final static String WORKSPACE_NAME = "rest-1";

    private static int m_startport = 9210;

    private int getNextPort() {
        return ++m_startport;
    }

    @Test
    public void testCreateResource() throws Exception {

        final int port = getNextPort();

        final Artifact artifact = new ArtifactBuilder()
            .isBundle()
            .setUrl("some://url")
            .setProcessorPid("myPid")
            .setName("name")
            .setDescription("description")
            .setBundleName("bundleName")
            .setBundleSymbolicName("bundleSymbolicName")
            .setBundleVersion("bundleVersion")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        final Mockery mockeryContext = new Mockery();
        final Handler aceClientServerHandler = mockeryContext.mock(Handler.class);
        mockeryContext.checking(new Expectations() {
            {
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateWorkspaceCreate(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCreate(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, artifact));
            }
        });

        SimpleHttpServer server = new SimpleHttpServer(port, aceClientServerHandler);
        server.startServer();
        try {
            AceClient client = new AceClient(SCHEME + "://" + HOST + ":" + port + "" + CLIENT_PATH);
            AceClientWorkspace w = client.createNewWorkspace();
            assertNotNull(w);

            String[] ids = w.createResources(new Artifact[] { artifact });
            assertEquals(1, ids.length);
        }
        finally {
            server.stopServer();
            mockeryContext.assertIsSatisfied();
        }
    }

    @Test
    public void testDeleteResource() throws Exception {

        final int port = getNextPort();

        final Class<? extends AbstractResource> artifactClass = Artifact.class;
        final String artifactId = "someArtifactID";

        final Mockery mockeryContext = new Mockery();
        final Handler aceClientServerHandler = mockeryContext.mock(Handler.class);
        mockeryContext.checking(new Expectations() {
            {
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateWorkspaceCreate(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceDelete(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, artifactClass, artifactId));
            }
        });

        SimpleHttpServer server = new SimpleHttpServer(port, aceClientServerHandler);
        server.startServer();
        try {
            AceClient client = new AceClient(SCHEME + "://" + HOST + ":" + port + "" + CLIENT_PATH);
            AceClientWorkspace w = client.createNewWorkspace();
            assertNotNull(w);
            w.deleteResource(Artifact.class, artifactId);
        }
        finally {
            server.stopServer();
            mockeryContext.assertIsSatisfied();
        }
    }

    @Test
    public void testGetResources() throws Exception {

        final int port = getNextPort();

        Mockery mockeryContext = new Mockery();
        final Handler aceClientServerHandler = mockeryContext.mock(Handler.class);
        mockeryContext.checking(new Expectations() {
            {
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateWorkspaceCreate(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCollectionGet(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, "artifact", "[]"));
            }
        });

        SimpleHttpServer server = new SimpleHttpServer(port, aceClientServerHandler);
        server.startServer();
        try {
            AceClient c = new AceClient(SCHEME + "://" + HOST + ":" + port + "" + CLIENT_PATH);
            AceClientWorkspace w = c.createNewWorkspace();
            assertNotNull(w);
            Artifact[] artifacts = w.getResources(Artifact.class);
            assertNotNull(artifacts);
        }
        finally {
            server.stopServer();
            mockeryContext.assertIsSatisfied();
        }
    }

    @Test
    public void testWorkspaceExport() throws Exception {

        final int port = getNextPort();

        Mockery mockeryContext = new Mockery();
        final Handler aceClientServerHandler = mockeryContext.mock(Handler.class);
        mockeryContext.checking(new Expectations() {
            {
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateWorkspaceCreate(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCollectionGet(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, "artifact", "[]"));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCollectionGet(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, "feature", "[]"));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCollectionGet(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, "distribution", "[]"));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCollectionGet(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, "target", "[]"));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCollectionGet(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, "artifact2feature", "[]"));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCollectionGet(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, "feature2distribution", "[]"));
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateResourceCollectionGet(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME, "distribution2target", "[]"));
            }
        });

        SimpleHttpServer server = new SimpleHttpServer(port, aceClientServerHandler);
        server.startServer();

        try {
            AceClient c = new AceClient(SCHEME + "://" + HOST + ":" + port + "" + CLIENT_PATH);
            AceClientWorkspace w = c.createNewWorkspace();
            assertNotNull(w);
            Model m = w.export();
            assertNotNull(m);
        }
        finally {
            server.stopServer();
            mockeryContext.assertIsSatisfied();
        }
    }
}
