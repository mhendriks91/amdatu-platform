/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tests.autoconf;

import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.inject.Inject;

import junit.framework.Assert;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.TestContext;
import org.amdatu.tenant.factory.TenantServiceFactory;
import org.apache.felix.fileinstall.ArtifactInstaller;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.TestProbeBuilder;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.junit.ProbeBuilder;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.ConfigurationListener;
import org.osgi.service.deploymentadmin.DeploymentPackage;
import org.osgi.service.deploymentadmin.spi.DeploymentSession;
import org.osgi.service.deploymentadmin.spi.ResourceProcessor;
import org.osgi.service.deploymentadmin.spi.ResourceProcessorException;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
@Ignore
public class AutoconfResourceProcessorTest {

    @Inject
    private BundleContext m_bundleContext;
    private TestContext m_testContext;
    private ResourceProcessor m_resourceProcessor;

    @Configuration
    public Option[] config() {
        return options(
            junitBundles(),
            CoreBundles.provisionAll(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            mavenBundle().groupId("org.apache.felix").artifactId("org.apache.felix.configadmin")
                .version("1.2.8")
                .start(false));
    }

//    @ProbeBuilder
//    public TestProbeBuilder overwriteHeaders(TestProbeBuilder probe) {
//        probe.setHeader("Bundle-ManifestVersion", "2");
//        probe.setHeader(
//          "Import-Package",
//          "org.osgi.service.deploymentadmin;managementagent=true;version=\"[1.0,2)\"," +
//          "org.osgi.service.deploymentadmin.spi;managementagent=true;version=\"[1.0,2)\"," +
//          "org.apache.ace.deployment.service;managementagent=true;version=\"[0.8,1)\"," +
//          "org.apache.felix.dm.tracker;managementagent=true;version=\"[3.0,4)\"," +
//          "org.osgi.service.cm;managementagent=true;version=\"[1.3,2)\"," +
//          "org.osgi.service.event;managementagent=true;version=\"[1.2,2)\"," +
//          "org.osgi.service.log;managementagent=true;version=\"[1.3,2)\"," +
//          "org.osgi.service.metatype;managementagent=true;version=\"[1.1,2)\"," +
//          "org.osgi.framework;version=\"[1.5,2)\"," +
//          "org.osgi.service.packageadmin;version=\"[1.2,2)\"," +
//          "org.osgi.util.tracker;version=\"[1.4,2)\""
//          );
//        return probe;
//    }

    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();

        restartAutoConfProcessor();
        useMultiTenantConfigAdmin(false);

        // not to fast to prevent concurrency issues
        Thread.sleep(1000);
    }

    @After
    public void tearDown() throws Exception {

        // not to fast to prevent concurrency issues
        Thread.sleep(1000);
        m_testContext.tearDown();
    }

    /**
     * Duplicate sessions should not be allowed
     */

    @Test(expected = IllegalStateException.class)
    public void expectAutoconfExceptionOnDuplicateSession() throws Exception {
        m_resourceProcessor.begin(new TestSession());
        m_resourceProcessor.begin(new TestSession());
    }

    /**
     * A rollback should reset the session
     */

    @Test
    public void expectAutoconfWeCanRestartAfterRollback() throws Exception {
        m_resourceProcessor.begin(new TestSession());
        m_resourceProcessor.rollback();
        m_resourceProcessor.begin(new TestSession());
    }

    /**
     * Some tests to check validation process
     */

    // AMDATU-572
    @Test(expected = ResourceProcessorException.class)
    public void expectExceptionWhenAttributesMissing() throws Exception {
        executeDeploy("resource", "faulty-NoAttributes.xml");
    }

    @Test(expected = ResourceProcessorException.class)
    public void expectExceptionWhenAttributeisInvalid() throws Exception {
        executeDeploy("resource", "faulty-IllegalAttribute.xml");
    }

    @Test(expected = ResourceProcessorException.class)
    public void expectExceptionWhenFilterisInvalid() throws Exception {
        executeDeploy("resource", "faulty-IllegalFilter.xml");
    }

    @Test(expected = ResourceProcessorException.class)
    public void expectExceptionWhenOCDRefIsMissing() throws Exception {
        executeDeploy("resource", "faulty-NoOCDRef.xml");
    }

    @Test(expected = ResourceProcessorException.class)
    public void expectExceptionWhenXMLNotWellFormed() throws Exception {
        executeDeploy("resource", "faulty-NotWellFormed.xml");
    }

    @Test(expected = ResourceProcessorException.class)
    public void expectExceptionWhenBundleLocationIsMissing() throws Exception {
        executeDeploy("resource", "faulty-BundleLocationMissing.xml");
    }

    @Test(expected = ResourceProcessorException.class)
    public void expectExceptionWhenObjectIsMissing() throws Exception {
        executeDeploy("resource", "faulty-NoObject.xml");
    }

    /**
     * Create & drop scenario with 1 resource containing 1 designate.
     */

    @Test
    public void testConfigurationSingleAddSingleDrop_StandardConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(false);
        ConfigEventMonitor cem = getConfigEventMonitor(null);
        try {
            testConfigurationSingleAddSingleDrop(cem, "tenant-1.xml");
        }
        finally {
            cem.destroy();
        }
    }

    @Test
    public void testConfigurationSingleAddSingleDrop_MultiTenantConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        try {
            testConfigurationSingleAddSingleDrop(cem, "tenant-1.xml");
        }
        catch (Exception e) {
            cem.destroy();

        }
        finally {
            cem.destroy();
        }
    }

    // FIXME: the MetaTypeService fails to parse the metatypefile in the tenant factory
    // org.xmlpull.v1.XmlPullParserException: unsupported feature: http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes
    // (position:START_DOCUMENT null@0:0 in bundle://12.0:0/OSGI-INF/metatype/metatype.xml)
    @Ignore
    @Test
    public void testConfigurationSingleAddSingleDrop_NoLocalOCD_StandardConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(false);
        ConfigEventMonitor cem = getConfigEventMonitor(null);
        try {
            testConfigurationSingleAddSingleDrop(cem, "tenant-1-noLocalOCD.xml");
        }
        finally {
            cem.destroy();
        }
    }

    @Ignore
    @Test
    public void testConfigurationSingleAddSingleDrop_NoLocalOCD_MultiTenantConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        try {
            testConfigurationSingleAddSingleDrop(cem, "tenant-1-noLocalOCD.xml");
        }
        finally {
            cem.destroy();
        }
    }

    private void testConfigurationSingleAddSingleDrop(ConfigEventMonitor cem, String resourceFile) throws Exception {
        executeDeploy("resource", resourceFile);
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);
        cem.reset();
        executeDroppped("resource");
        cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 1);
    }

    /**
     * Create & drop scenario with 1 resource containing 20 designates.
     *
     * FIXME these test frequently result in
     * java.lang.IllegalStateException Could not find component for tenant tenant1
     */

    @Test
    public void testConfigurationManyAddSingleDrop_StandardConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(false);
        ConfigEventMonitor cem = getConfigEventMonitor(null);
        try {
            testConfigurationManyAddSingleDrop(cem, "tenant-1-20.xml");
        }
        finally {
            cem.destroy();
        }
    }

    @Test
    public void testConfigurationManyAddSingleDrop_MultiTenantConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        try {
            testConfigurationManyAddSingleDrop(cem, "tenant-1-20.xml");
        }
        finally {
            cem.destroy();
        }
    }

    private void testConfigurationManyAddSingleDrop(ConfigEventMonitor cem, String resourceFile) throws Exception {
        executeDeploy("resource", resourceFile);
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 20);
        cem.reset();
        executeDroppped("resource");
        cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 20);
    }

    /**
     * Create, update & drop scenario with 1 resource containing 1 designate.
     */

    @Test
    public void testConfigurationSingleAddSingleUpdateDrop_StandardConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(false);
        ConfigEventMonitor cem = getConfigEventMonitor(null);
        try {
            testConfigurationSingleAddSingleUpdateDrop(cem);
        }
        finally {
            cem.destroy();
        }
    }

    @Test
    public void testConfigurationSingleAddSingleUpdateDrop_MultiTenantConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        try {
            testConfigurationSingleAddSingleUpdateDrop(cem);
        }
        finally {
            cem.destroy();
        }
    }

    private void testConfigurationSingleAddSingleUpdateDrop(ConfigEventMonitor cem) throws Exception {
        executeDeploy("resource", "tenant-1.xml");
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);
        cem.reset();
        executeDeploy("resource", "tenant-2.xml");
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);
        cem.reset();
        executeDroppped("resource");
        cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 1);
    }

    /**
     * Create & dropall scenario with 2 resources containing 1 designate each.
     */

    @Test
    public void testConfigurationSingleAddSingleAddDrop_StandardConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(false);
        ConfigEventMonitor cem = getConfigEventMonitor(null);
        try {
            testConfigurationSingleAddSingleAddDrop(cem);
        }
        finally {
            cem.destroy();
        }
    }

    @Test
    public void testConfigurationSingleAddSingleAddDrop_MultiTenantConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        try {
            testConfigurationSingleAddSingleAddDrop(cem);
        }
        finally {
            cem.destroy();
        }
    }

    private void testConfigurationSingleAddSingleAddDrop(ConfigEventMonitor cem) throws Exception {
        executeDeploy("resource1", "tenant-1.xml");
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);
        cem.reset();
        executeDeploy("resource2", "tenant-2.xml");
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);
        cem.reset();
        executeDropAll();
        cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 2);
    }

    /**
     * Create & dropall scenario with 1 resources containing 2 designates.
     */

    @Test
    public void testConfigurationMultiAddDropAll_StandardConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(false);
        ConfigEventMonitor cem = getConfigEventMonitor(null);
        try {
            testConfigurationMultiAddDropAll(cem);
        }
        finally {
            cem.destroy();
        }
    }

    @Test
    public void testConfigurationMultiAddDropAll_MultiTenantConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        try {
            testConfigurationMultiAddDropAll(cem);
        }
        finally {
            cem.destroy();
        }
    }

    private void testConfigurationMultiAddDropAll(ConfigEventMonitor cem) throws Exception {
        executeDeploy("resource", "tenant-1_2.xml");
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 2);
        cem.reset();
        executeDropAll();
        cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 2);
    }

    /**
     * Create, update & dropall scenario with 1 resources initially contain 1 designate and 2 on update.
     */

    @Test
    public void testConfigurationSingleAddMultiUpdateDropAll_StandardConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(false);
        ConfigEventMonitor cem = getConfigEventMonitor(null);
        try {
            testConfigurationSingleAddMultiUpdateDropAll(cem);
        }
        finally {
            cem.destroy();
        }
    }

    @Test
    public void testConfigurationSingleAddMultiUpdateDropAll_MultiTenantConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        try {
            testConfigurationSingleAddMultiUpdateDropAll(cem);
        }
        finally {
            cem.destroy();
        }
    }

    private void testConfigurationSingleAddMultiUpdateDropAll(ConfigEventMonitor cem) throws Exception {
        executeDeploy("resource", "tenant-1.xml");
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);
        cem.reset();
        executeDeploy("resource", "tenant-1_2.xml");
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 2);
        cem.reset();
        executeDropAll();
        cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 2);
    }

    /**
     * Create, update & dropall scenario with 1 resources initially containing 2 designates and 1 on update.
     */

    @Test
    public void testConfigurationMultiAddSingleUpdateDropAll_StandardConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(false);
        ConfigEventMonitor cem = getConfigEventMonitor(null);
        try {
            testConfigurationMultiAddSingleUpdateDropAll(cem);
        }
        finally {
            cem.destroy();
        }
    }

    @Test
    public void testConfigurationMultiAddSingleUpdateDropAll_MultiTenantConfigAdmin() throws Exception {
        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        try {
            testConfigurationMultiAddSingleUpdateDropAll(cem);
        }
        finally {
            cem.destroy();
        }
    }

    private void testConfigurationMultiAddSingleUpdateDropAll(ConfigEventMonitor cem) throws Exception {
        executeDeploy("resource", "tenant-1_2.xml");
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 2);
        cem.reset();
        executeDeploy("resource", "tenant-1.xml");
        cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 1);
        cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);
        cem.reset();
        executeDropAll();
        cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 1);
    }

    /**
     * MultiTenant Test in logical order:
     *
     * 0) Deploy a "platform" configuration for the TenantServiceFactry
     * 1) Autoconf delivers the configuration to the ConfigurationAdmin ("platform")
     * 2) ConfigurationAdmin ("platform") will deliver the configuration to the TenantServiceFactory
     * 3) TenantServiceFactory will register a Tenant service ("tenant1")
     * 4) ConfigurationAdmin TenantAdaptor will register a ConfigurationAdmin ("tenant1")
     * 5) Deploy a "tenant1" configuration for the whatever
     * 6) Autoconf delivers the configuration to the ConfigurationAdmin ("tenant1")
     *
     * Assert that the proper ConfigurationEvent are throw by the correct ConfigurationAdmin
     * services.
     */

    @Test
    public void testMultiTenantTenantConfigurationSingleAddDropAll_InOrder() throws Exception {

        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem1 = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        ConfigEventMonitor cem2 = getConfigEventMonitor("tenant1");

        try {
            executeDeploy("resource", "tenant-1.xml");
            cem1.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);

            executeDeploy("resource2", "tenant-1-conf.xml");
            cem2.assertPidUpdated("foo.bar.pid", 1);
            cem1.assertFactoryPidUpdated(TenantServiceFactory.PID, 0);
            cem1.assertFactoryPidDeleted(TenantServiceFactory.PID, 0);

            cem1.reset();
            executeDroppped("resource2");
            cem2.assertPidDeleted("foo.bar.pid", 1);
            executeDroppped("resource");
            cem1.assertFactoryPidDeleted(TenantServiceFactory.PID, 1);
        }
        finally {
            cem1.destroy();
            cem2.destroy();
        }
    }

    /**
     * MultiTenant Test out of order:
     *
     * 0) Deploy a "tenant1" configuration for the whatever
     * 1) Deploy a "platform" configuration for the TenantServiceFactry
     * 2) Autoconf delivers the configuration to the ConfigurationAdmin ("platform")
     * 3) ConfigurationAdmin ("platform") will deliver the configuration to the TenantServiceFactory
     * 4) TenantServiceFactory will register a Tenant service ("tenant1")
     * 5) ConfigurationAdmin TenantAdaptor will register a ConfigurationAdmin ("tenant1")
     * 6) Autoconf delivers the configuration to the ConfigurationAdmin ("tenant1")
     *
     * Assert that the proper ConfigurationEvent are throw by the correct ConfigurationAdmin
     * services.
     *
     * FIXME this fails every now and then! there is a nasty race condition or something... :(
     */

    @Test
    public void testMultiTenantTenantConfigurationSingleAddDropAll_OutOfOrder() throws Exception {

        useMultiTenantConfigAdmin(true);
        ConfigEventMonitor cem1 = getConfigEventMonitor(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM);
        ConfigEventMonitor cem2 = getConfigEventMonitor("tenant1");

        try {
            executeDeploy("resource2", "tenant-1-conf.xml");
            Thread.sleep(1000);
            executeDeploy("resource", "tenant-1.xml");
            Thread.sleep(1000);
            cem2.assertPidUpdated("foo.bar.pid", 1);
            cem1.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);

            cem1.reset();
            executeDropAll();
            cem1.assertFactoryPidDeleted(TenantServiceFactory.PID, 1);
        }
        finally {
            cem1.destroy();
            cem2.destroy();
        }
    }

    /**
     * Install, update & uninstall scenario all the way through the autoconf Fileinstall ArtifactInstaller.
     */

    @Test
    public void testFileInstallAutoConfExtensionDelivery() throws Exception {

        ConfigEventMonitor cem = getConfigEventMonitor(null);
        useMultiTenantConfigAdmin(false);

        ArtifactInstaller installer =
            m_testContext.getService(ArtifactInstaller.class, "(org.amdatu.fileinstall.autoconf=true)");
        Assert.assertNotNull("Can't find autoconf artifact installer", installer);

        try {
            File configFile = copyResourceToTempFile("tenant-1.xml");

            Assert.assertTrue(installer.canHandle(configFile));

            installer.install(configFile);
            cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 1);
            cem.reset();

            configFile = copyResourceToTempFile("tenant-1_2.xml");
            Assert.assertTrue(installer.canHandle(configFile));

            installer.update(configFile);
            cem.assertFactoryPidUpdated(TenantServiceFactory.PID, 2);

            installer.uninstall(configFile);
            cem.assertFactoryPidDeleted(TenantServiceFactory.PID, 2);
        }
        finally {
            cem.destroy();
        }
    }

    private File copyResourceToTempFile(String resource) throws Exception {
        File tmp = null;
        URL url = null;
        InputStream is = null;
        OutputStream os = null;
        try {
            tmp = File.createTempFile("autoconftest-", ".xml");
            url = m_bundleContext.getBundle().getResource(resource);
            is = url.openStream();
            os = new FileOutputStream(tmp);
            byte buf[] = new byte[1024];
            int len;
            while ((len = is.read(buf)) > 0) {
                os.write(buf, 0, len);
            }
            return tmp;
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                }
                finally {
                    if (os != null) {
                        os.close();
                    }
                }
            }
        }
    }

    private void executeDeploy(String resourceName, String fileName) throws Exception {
        URL url = m_bundleContext.getBundle().getResource(fileName);

        m_resourceProcessor.begin(new TestSession());
        m_resourceProcessor.process(resourceName, url.openStream());
        m_resourceProcessor.prepare();
        m_resourceProcessor.commit();
        sendDeploymentCompleteEvent();
    }

    private void executeDroppped(String resourceName) throws Exception {
        m_resourceProcessor.begin(new TestSession());
        m_resourceProcessor.dropped(resourceName);
        m_resourceProcessor.prepare();
        m_resourceProcessor.commit();
        sendDeploymentCompleteEvent();
    }

    private void executeDropAll() throws Exception {
        m_resourceProcessor.begin(new TestSession());
        m_resourceProcessor.dropAllResources();
        m_resourceProcessor.prepare();
        m_resourceProcessor.commit();
        sendDeploymentCompleteEvent();
    }

    private void useMultiTenantConfigAdmin(boolean useMultiTenantConfigAdmin) throws Exception {
        Bundle[] bundles = m_bundleContext.getBundles();
        for (Bundle bundle : bundles) {
            if (bundle.getSymbolicName().equals("org.apache.felix.configadmin")) {
                if (useMultiTenantConfigAdmin) {
                    bundle.stop();
                }
                else {
                    bundle.start();
                }
            }
            else {
                if (bundle.getSymbolicName().equals("org.amdatu.multitenant.org.apache.felix.configadmin")) {
                    if (useMultiTenantConfigAdmin) {
                        bundle.start();
                    }
                    else {
                        bundle.stop();
                    }
                }
            }
        }
        m_testContext.getService(ConfigurationAdmin.class);
    }

    private void restartAutoConfProcessor() throws Exception {
        Bundle[] bundles = m_bundleContext.getBundles();
        for (Bundle bundle : bundles) {
            if (bundle.getSymbolicName().equals("org.amdatu.deployment.autoconf")) {
                bundle.stop();
                bundle.start();
            }
        }
        m_resourceProcessor =
            m_testContext.getService(ResourceProcessor.class, "(service.pid=org.osgi.deployment.rp.autoconf)");
    }

    private void sendDeploymentCompleteEvent() throws Exception {
        Map properties = new HashMap();
        properties.put("successful", Boolean.TRUE);
        Event complete = new Event("org/osgi/service/deployment/COMPLETE", properties);
        EventAdmin eventAdmin = m_testContext.getService(EventAdmin.class);
        if (eventAdmin == null) {
            throw new IllegalStateException("Could not find an EventAdmin service to send deployment complete event to.");
        }
        eventAdmin.sendEvent(complete);
    }

    private ConfigEventMonitor getConfigEventMonitor(String tenantPid) {
        return new ConfigEventMonitor(m_bundleContext, tenantPid);
    }

    static class TestSession implements DeploymentSession {

        public DeploymentPackage getTargetDeploymentPackage() {
            return null;
        }

        public DeploymentPackage getSourceDeploymentPackage() {
            return null;
        }

        public File getDataFile(Bundle bundle) {
            return null;
        }
    }

    static class ConfigEventMonitor implements ConfigurationListener {

        public static final long DEFAULT_TIMEOUT = 10000;

        private final List<ConfigurationEvent> m_eventList = new LinkedList<ConfigurationEvent>();

        private final ReentrantReadWriteLock m_evenListLock = new ReentrantReadWriteLock(true);

        private final String m_tenantId;

        private ServiceRegistration m_svcRegistration;

        public ConfigEventMonitor(BundleContext context, String tenantPid) {
            m_tenantId = tenantPid;
            Dictionary<String, Object> props = new Hashtable<String, Object>();
            if (tenantPid != null && !tenantPid.equals("")) {
                props.put(org.amdatu.tenant.Constants.PID_KEY, tenantPid);
            }
            m_svcRegistration = context.registerService(ConfigurationListener.class.getName(), this, props);
        }

        public void reset() {
            m_evenListLock.writeLock().lock();
            try {
                m_eventList.clear();
            }
            finally {
                m_evenListLock.writeLock().unlock();
            }
        }

        public void destroy() {
            if (m_svcRegistration != null) {
                m_svcRegistration.unregister();
            }
        }

        public void configurationEvent(ConfigurationEvent event) {
            m_evenListLock.writeLock().lock();
            try {
                m_eventList.add(event);
            }
            finally {
                m_evenListLock.writeLock().unlock();
            }
        }

        public void assertPidUpdated(String pid, int count) {
            assertEvents(pid, ConfigurationEvent.CM_UPDATED, false, count, DEFAULT_TIMEOUT);
        }

        public void assertPidDeleted(String pid, int count) throws InterruptedException {
            assertEvents(pid, ConfigurationEvent.CM_DELETED, false, count, DEFAULT_TIMEOUT);
        }

        public void assertFactoryPidUpdated(String pid, int count) throws InterruptedException {
            assertEvents(pid, ConfigurationEvent.CM_UPDATED, true, count, DEFAULT_TIMEOUT);
        }

        public void assertFactoryPidDeleted(String pid, int count) throws InterruptedException {
            assertEvents(pid, ConfigurationEvent.CM_DELETED, true, count, DEFAULT_TIMEOUT);
        }

        public void assertEvents(final String pid, final int type, final boolean isFactoryPid,
            final int count, final long timeout) {

            long start = System.currentTimeMillis();
            int counted = 0;
            while (counted < count && timeout > (System.currentTimeMillis() - start)) {
                counted = 0;
                m_evenListLock.readLock().lock();
                try {
                    for (ConfigurationEvent event : m_eventList) {
                        if (event.getType() == type
                            && (isFactoryPid ? (event.getFactoryPid() != null && event.getFactoryPid().equals(pid))
                                : (event.getPid() != null && event.getPid().equals(pid)))) {
                            counted++;
                        }
                    }
                }
                finally {
                    m_evenListLock.readLock().unlock();
                    try {
                        Thread.sleep(10);
                    }
                    catch (InterruptedException e) {}
                }
            }
            if (counted != count) {
                Assert.fail(m_tenantId + " Did not receive " + count
                    + (type == ConfigurationEvent.CM_DELETED ? " DELETED" : " UPDATED")
                    + " configuration events for" + (isFactoryPid ? " factoryPid " : "Pid ") + pid);
            }
        }
    }
}