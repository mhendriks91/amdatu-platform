/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.maven.plugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Adds/updates sourceheaders from a file on all .java files found in the
 * specified (test)sourceDirectories of project.
 * 
 * @author <a href="http://www.amdatu.org">Amdatu</a>
 * @goal updateHeaders
 */
public class ClassHeadersMojo extends AbstractMojo {

    // @checkstyle:off

    // Member names do not conform to checkstyle criteria cause
    // maven uses reflection to map them.

    /**
     * @parameter expression="${project.build.sourceDirectory}"
     */
    private File sourceDirectory;

    /**
     * @parameter expression="${project.build.testSourceDirectory}"
     */
    private File testSourceDirectory;

    // @checkstyle:on

    public void execute() throws MojoExecutionException {

        try {
            InputStream is = ClassHeadersMojo.class.getClassLoader().getResourceAsStream("Apache-License-2.0.header");
            StringWriter writer = new StringWriter();
            InputStreamReader reader = new InputStreamReader(is);
            IOUtils.copy(reader, writer);
            String headerString = writer.toString();

            if (sourceDirectory != null
                && (!sourceDirectory.exists() || !sourceDirectory.isDirectory() || !sourceDirectory.canRead())) {
                getLog()
                    .warn("Configured sourceDirectory not accessible... skipping: "
                        + sourceDirectory.getAbsolutePath());
            }
            else {
                processDirectory(headerString, sourceDirectory);
            }

            if (testSourceDirectory != null
                && (!testSourceDirectory.exists() || !testSourceDirectory.isDirectory() || !testSourceDirectory
                    .canRead())) {
                getLog().warn(
                    "Configured testSourceDirectory not accessible... skipping: "
                        + testSourceDirectory.getAbsolutePath());
            }
            else {
                processDirectory(headerString, testSourceDirectory);
            }
        }
        catch (IOException e) {
            throw new MojoExecutionException("Failed to read configured headerFile", e);
        }
    }

    private void processDirectory(String headerString, File directory) throws MojoExecutionException {

        Collection<File> sourceFiles = FileUtils.listFiles(directory, new String[] {"java"}, true);
        Iterator<File> sourceFilesIterator = sourceFiles.iterator();

        getLog().info("Processing " + sourceFiles.size() + " sourcefiles in directory " + directory.getAbsolutePath());

        while (sourceFilesIterator.hasNext()) {
            File sourceFile = sourceFilesIterator.next();
            String sourceString;
            try {
                sourceString = FileUtils.readFileToString(sourceFile, "UTF-8");
                // Note that the string "package" is split so that JXR 2.0
                // doesn't interpret it as a real package token. This bug was
                // fixed in JXR in revision 516976
                // see: http://svn.apache.org/viewvc?view=rev&revision=516976
                int index = sourceString.indexOf("pac" + "kage ");
                if (index == -1) {
                    index = sourceString.indexOf("im" + "port ");
                }
                if (index == -1) {
                    getLog().info("Couldn't locate package or import keyword in file: " + sourceFile.getAbsolutePath());
                }
                else {
                    FileUtils.writeStringToFile(sourceFile, headerString + sourceString.substring(index), "UTF-8");
                }
            }
            catch (IOException e) {
                throw new MojoExecutionException("Exception while processing sourceFile: "
                    + sourceFile.getAbsolutePath(), e);
            }
        }
    }
}
