/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink.osgi;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.web.rest.wink.service.WinkRegistrationServiceImpl;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

/**
 * This is the OSGi activator for the Amdatu REST framework based on Apache Wink.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {

    public static final String CONTEXTID = "amdatu-rest";
    public static final String DEFAULT_ALIAS = "";

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        Dictionary<String, String> props = new Hashtable<String, String>();
        props.put(Constants.SERVICE_PID, WinkRegistrationServiceImpl.CONFIG_PID);
        props.put("org.amdatu.tenant.pid", "org.amdatu.tenant.PLATFORM");

        manager.add(createComponent()
            .setAutoConfig(Component.class, Boolean.FALSE)
            .setInterface(ManagedService.class.getName(), props)
            .setImplementation(WinkRegistrationServiceImpl.class)
            .add(createServiceDependency()
                .setService("(" + Constants.OBJECTCLASS + "=*)")
                .setCallbacks("onAdded", "onRemoved")
                .setRequired(false))
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false))
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
