/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resource.service;

import static org.amdatu.tenant.Constants.PID_KEY;
import static org.amdatu.web.dispatcher.Constants.ALIAS_KEY;
import static org.amdatu.web.dispatcher.Constants.CONTEXT_ID_KEY;
import static org.amdatu.web.resource.Constants.RESOURCE_ALIAS_KEY;
import static org.osgi.framework.Constants.SERVICE_ID;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.Servlet;

import org.amdatu.web.httpcontext.ResourceProvider;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * This class is responsible for registration of Resource Servlets for each
 * ResourceProvider that comes available.
 */
public class ResourceProviderListener {

    private final ConcurrentHashMap<ServiceReference, Component> m_components =
        new ConcurrentHashMap<ServiceReference, Component>();

    private volatile DependencyManager m_dependencyManager;
    private volatile LogService m_logService;

    public void stop() {
        for (Component component : m_components.values()) {
            m_dependencyManager.remove(component);
        }
        m_components.clear();
    }

    public void resourceProviderAdded(ServiceReference serviceReference, ResourceProvider resourceProvider) {

        final long serviceId = getLongProperty(serviceReference, SERVICE_ID);

        final String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId == null || "".equals(contextId)) {
            m_logService.log(LogService.LOG_WARNING, "ResourceProvider(" + serviceId
                + ") does not specify a contextId. Ignoring..");
            return;
        }

        final String resourceAlias = getStringProperty(serviceReference, RESOURCE_ALIAS_KEY);
        if (resourceAlias == null || "".equals(resourceAlias)) {
            m_logService.log(LogService.LOG_WARNING, "ResourceProvider(" + serviceId
                + ") does not specify a resourceAlias. Ignoring..");
            return;
        }

        final Component resourceServletComponent = createResourceServletComponent(serviceReference);
        if (m_components.putIfAbsent(serviceReference, resourceServletComponent) != null) {
            m_logService.log(LogService.LOG_WARNING, "ResourceProvider(" + serviceId
                + ") specifies already registered resourceAlias. Ignoring..");
            return;
        }
        m_dependencyManager.add(resourceServletComponent);
        m_logService.log(LogService.LOG_DEBUG, "Registered Resource servlet at " + resourceAlias
            + " for ResourceProvider("
            + serviceId + ").");
    }

    public void resourceProviderRemoved(ServiceReference serviceReference, ResourceProvider resourceProvider) {

        final long serviceId = getLongProperty(serviceReference, SERVICE_ID);
        final String jspAlias = getStringProperty(serviceReference, RESOURCE_ALIAS_KEY);

        final Component resourceServletComponent = m_components.remove(serviceReference);
        if (resourceServletComponent == null) {
            m_logService.log(LogService.LOG_DEBUG, "No JSP servlet registered for ResourceProvider(" + serviceId
                + "). Ignoring..");
            return;
        }
        m_dependencyManager.remove(resourceServletComponent);
        m_logService.log(LogService.LOG_DEBUG, "Removed JSP servlet at " + jspAlias + " for ResourceProvider("
            + serviceId + ").");
    }

    private Component createResourceServletComponent(final ServiceReference serviceReference) {

        Dictionary<String, Object> properties = new Hashtable<String, Object>();

        final String resourceAlias = getStringProperty(serviceReference, RESOURCE_ALIAS_KEY);
        properties.put(ALIAS_KEY, resourceAlias);

        final String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (properties != null) {
            properties.put(CONTEXT_ID_KEY, contextId);
        }

        // AMDATU-479 - this is tricky
        final String tenantPID = getStringProperty(serviceReference, PID_KEY);
        if (tenantPID != null) {
            properties.put(PID_KEY, tenantPID);
        }

        Component servletComponent = m_dependencyManager.createComponent();
        servletComponent.setInterface(Servlet.class.getName(), properties);
        servletComponent.setImplementation(new ResourceServlet());
        return servletComponent;
    }

    private Long getLongProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof Long) ? (Long) value : -1;
    }

    private String getStringProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof String) ? (String) value : null;
    }
}
