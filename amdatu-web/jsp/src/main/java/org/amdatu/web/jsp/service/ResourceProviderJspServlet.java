/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.jsp.service;

import java.io.IOException;
import java.util.concurrent.Callable;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.amdatu.web.httpcontext.ResourceProvider;
import org.apache.jasper.Constants;
import org.apache.jasper.servlet.JspServlet;
import org.ops4j.pax.swissbox.core.ContextClassLoaderUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * This class implements a servlet that facilitates rendering and compiling JSPs for a particular resource provider.
 * It extends the Jasper implementation of the JSPServlet but uses OPS4J Swissbox to fix some classloading issues with
 * this servlet.
 * This class was copied and modified from org.ops4j.pax.web.jsp.JspServletWrapper.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ResourceProviderJspServlet implements Servlet {

    // Injected by the OSGi container
    private volatile LogService m_logService;

    // The Jasper Servlet
    private JspServlet m_jasperServlet;

    // / Jasper specific class loader.
    private JasperClassLoader m_jasperClassLoader;

    // The resource provider that provides the JSPs tro be rendered by this servlet
    private ResourceProvider m_resourceProvider;

    private BundleContext m_bundleContext;

    private Bundle m_bundle;

    // Constructor
    public ResourceProviderJspServlet(Bundle bundle) {
        m_bundle = bundle;
    }

    public void init2() {
        ClassLoader jcl = JasperClassLoader.class.getClassLoader();
        m_jasperClassLoader = new JasperClassLoader(m_bundle, jcl);
        m_jasperClassLoader.addClassPathJars(m_bundleContext.getBundle());
        m_jasperServlet = new JspServlet();
    }

    /**
     * Delegates to jasper servlet with a controlled context class loader.
     * 
     * @see ResourceProviderJspServlet#init(ServletConfig)
     */
    public void init(final ServletConfig config) throws ServletException {
        try {
            ContextClassLoaderUtils.doWithClassLoader(
                m_jasperClassLoader,
                new Callable<Void>() {
                    public Void call() throws Exception {
                        m_jasperServlet.init(config);
                        return null;
                    }
                }
                );
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_ERROR, "Could not initialize Jsp Servlet", e);
            throw new ServletException(e);
        }
    }

    /**
     * Delegates to jasper servlet.
     * 
     * @see ResourceProviderJspServlet#getServletConfig()
     */
    public ServletConfig getServletConfig() {
        return m_jasperServlet.getServletConfig();
    }

    /**
     * Delegates to Jasper servlet with a controlled context class loader.
     * 
     * @see ResourceProviderJspServlet#service(ServletRequest, ServletResponse)
     */
    public void service(final ServletRequest req, final ServletResponse res) throws ServletException, IOException {

        final HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        String target = httpServletRequest.getRequestURI();
        if (!httpServletRequest.getContextPath().equals("")) {
            target = target.replaceFirst(httpServletRequest.getContextPath(), "");
        }
        req.setAttribute(Constants.JSP_FILE, target);

        try {
            ContextClassLoaderUtils.doWithClassLoader(
                m_jasperClassLoader,
                new Callable<Void>() {
                    public Void call() throws Exception {
                        m_jasperServlet.service(req, res);
                        return null;
                    }
                }
                );
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_ERROR, "An error occurred while handling JSP request", e);
            throw new ServletException(e);
        }
    }

    /**
     * Delegates to jasper servlet.
     * 
     * @see ResourceProviderJspServlet#getServletInfo()
     */
    public String getServletInfo() {
        return m_jasperServlet.getServletInfo();
    }

    /**
     * Delegates to jasper servlet with a controlled context class loader.
     * 
     * @see ResourceProviderJspServlet#destroy()
     */
    public void destroy() {
        try {
            ContextClassLoaderUtils.doWithClassLoader(
                m_jasperClassLoader,
                new Callable<Void>() {
                    public Void call() throws Exception {
                        m_jasperServlet.destroy();
                        return null;
                    }
                }
                );
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_ERROR, "Could not destroy JspServletServiceImpl", e);
        }
    }

    public ResourceProvider getResourceProvider() {
        return m_resourceProvider;
    }
}
