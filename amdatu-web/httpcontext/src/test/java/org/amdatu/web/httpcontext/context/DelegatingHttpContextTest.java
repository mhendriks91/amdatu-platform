/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.httpcontext.context;

import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.amdatu.web.httpcontext.MimeTypeResolver;
import org.amdatu.web.httpcontext.ResourceProvider;
import org.amdatu.web.httpcontext.SecurityHandler;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

public class DelegatingHttpContextTest {

    @Test
    public void testResourceProviderDelegation() throws Exception {

        final Mockery mockContext = new Mockery();
        final DelegatingHttpContext httpContext = new DelegatingHttpContext("test-context");
        final ResourceProvider provider1 = mockContext.mock(ResourceProvider.class, "mock1");
        final ResourceProvider provider2 = mockContext.mock(ResourceProvider.class, "mock2");
        httpContext.addResourceProvider(provider1);
        httpContext.addResourceProvider(provider2);

        mockContext.checking(new Expectations() {
            {
                one(provider1).getResource(with("file.txt"));
                will(returnValue(null));
                one(provider2).getResource(with("file.txt"));
                will(returnValue(new URL("http", "whatever", "file.txt")));
            }
        });

        Assert.assertNotNull(httpContext.getResource("file.txt"));
        mockContext.assertIsSatisfied();
    }

    @Test
    public void testMimetypeResolverDelegation() throws Exception {

        final Mockery mockContext = new Mockery();
        final DelegatingHttpContext httpContext = new DelegatingHttpContext("test-context");
        final MimeTypeResolver resolver1 = mockContext.mock(MimeTypeResolver.class, "mock1");
        final MimeTypeResolver resolver2 = mockContext.mock(MimeTypeResolver.class, "mock2");
        httpContext.addMimeTypeResolver(resolver1);
        httpContext.addMimeTypeResolver(resolver2);

        mockContext.checking(new Expectations() {
            {
                one(resolver1).getMimetype((with("file.xyz")));
                will(returnValue(null));
                one(resolver2).getMimetype((with("file.xyz")));
                will(returnValue("bla/xyz"));
            }
        });

        Assert.assertEquals("bla/xyz", httpContext.getMimeType("file.xyz"));
        mockContext.assertIsSatisfied();
    }

    @Test
    public void testSecurityHandlerDelegation() throws Exception {

        final Mockery mockContext = new Mockery();
        final DelegatingHttpContext httpContext = new DelegatingHttpContext("test-context");
        final SecurityHandler resolver1 = mockContext.mock(SecurityHandler.class, "mock1");
        final SecurityHandler resolver2 = mockContext.mock(SecurityHandler.class, "mock2");
        final HttpServletRequest request = mockContext.mock(HttpServletRequest.class);
        final HttpServletResponse response = mockContext.mock(HttpServletResponse.class);
        httpContext.addSecurityHandler(resolver1);
        httpContext.addSecurityHandler(resolver2);

        mockContext.checking(new Expectations() {
            {
                one(resolver1).handleSecurity(request, response);
                will(returnValue(true));
                one(resolver2).handleSecurity(request, response);
                will(returnValue(false));
            }
        });

        Assert.assertFalse(httpContext.handleSecurity(request, response));
        mockContext.assertIsSatisfied();
    }
}
