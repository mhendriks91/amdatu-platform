/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.httpcontext.context;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.httpcontext.MimeTypeResolver;
import org.amdatu.web.httpcontext.ResourceProvider;
import org.amdatu.web.httpcontext.SecurityHandler;
import org.apache.felix.http.base.internal.util.MimeTypes;
import org.osgi.service.http.HttpContext;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class DelegatingHttpContext implements HttpContext {

    private final CopyOnWriteArrayList<SecurityHandler> m_securityHandlers =
        new CopyOnWriteArrayList<SecurityHandler>();
    private final CopyOnWriteArrayList<ResourceProvider> m_resourceProviders =
        new CopyOnWriteArrayList<ResourceProvider>();
    private final CopyOnWriteArrayList<MimeTypeResolver> m_mimeTypeResolvers =
        new CopyOnWriteArrayList<MimeTypeResolver>();

    private final String m_id;

    public DelegatingHttpContext(String id) {
        m_id = id;
    }

    public String getId() {
        return m_id;
    }

    public boolean addSecurityHandler(SecurityHandler securityHandler) {
        return m_securityHandlers.addIfAbsent(securityHandler);
    }

    public boolean removeSecurityHandler(SecurityHandler securityHandler) {
        return m_securityHandlers.remove(securityHandler);
    }

    public boolean addResourceProvider(ResourceProvider resourceProvider) {
        return m_resourceProviders.addIfAbsent(resourceProvider);
    }

    public boolean removeResourceProvider(ResourceProvider resourceProvider) {
        return m_resourceProviders.remove(resourceProvider);
    }

    public boolean addMimeTypeResolver(MimeTypeResolver mimeTypeResolver) {
        return m_mimeTypeResolvers.addIfAbsent(mimeTypeResolver);
    }

    public boolean removeMimeTypeResolver(MimeTypeResolver mimeTypeResolver) {
        return m_mimeTypeResolvers.remove(mimeTypeResolver);
    }

    public boolean handleSecurity(HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse) throws IOException {
        for (SecurityHandler securityHandler : m_securityHandlers) {
            if (!securityHandler.handleSecurity(httpServletRequest,
                httpServletResponse)) {
                return false;
            }
        }
        return true;
    }

    public URL getResource(String name) {
        for (ResourceProvider resourceProvider : m_resourceProviders) {
            URL resourceUrl = resourceProvider.getResource(name);
            if (resourceUrl != null) {
                return resourceUrl;
            }
        }
        return null;
    }

    public String getMimeType(String name) {
        for (MimeTypeResolver mimeTypeResolver : m_mimeTypeResolvers) {
            String mimeType = mimeTypeResolver.getMimetype(name);
            if (mimeType != null) {
                return mimeType;
            }
        }
        return MimeTypes.get().getByFile(name);
    }
}
