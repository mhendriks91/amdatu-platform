/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.httpcontext;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Service interface for a delegate to a named <code>DelegatingHttpContext</code>.
 * The service must be registered with the relevant contextId property.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface SecurityHandler {

    /**
     * Handles security for the specified request. This method controls whether
     * the request is processed in the normal manner or an error is returned.
     * 
     * @param httpServletRequest
     *        the HTTP request
     * @param httpServletResponse
     *        the HTTP response
     * @return true if the request should be serviced, false if the request
     *         should not be serviced.
     * @throws IOException
     *         may be thrown by this method. If this occurs, the request is
     *         terminated and socket closed.
     */
    boolean handleSecurity(HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse) throws IOException;
}
