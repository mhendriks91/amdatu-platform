/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.httpcontext.service;

import static org.amdatu.web.dispatcher.Constants.CONTEXT_ID_KEY;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.web.httpcontext.MimeTypeResolver;
import org.amdatu.web.httpcontext.ResourceProvider;
import org.amdatu.web.httpcontext.SecurityHandler;
import org.amdatu.web.httpcontext.context.DelegatingHttpContext;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpContext;
/**
 * The {@code}HttpContextManagerService{@code} service is responsible for managing
 * delegating HttpContext services.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class HttpContextManagerServiceImpl {

    private final ConcurrentHashMap<String, ContextComponentTuple> m_httpContextComponents =
        new ConcurrentHashMap<String, ContextComponentTuple>();

    private volatile DependencyManager m_dependencyManager;

    public void start() {
        m_httpContextComponents.clear();
    }

    public void addResourceProvider(ServiceReference serviceReference, Object service) {
        ResourceProvider resourceProvider = (ResourceProvider) service;
        String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId != null) {
            ContextComponentTuple tuple = getOrCreateContextComponentTuple(contextId);
            tuple.getContext().addResourceProvider(resourceProvider);
        }
    }

    public void removeResourceProvider(ServiceReference serviceReference, Object service) {
        ResourceProvider resourceProvider = (ResourceProvider) service;
        String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId != null) {
            ContextComponentTuple tuple = m_httpContextComponents.get(contextId);
            if (tuple != null) {
                tuple.getContext().removeResourceProvider(resourceProvider);
            }
        }
    }

    public void addSecurityHandler(ServiceReference serviceReference, Object service) {
        SecurityHandler securityHandler = (SecurityHandler) service;
        String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId != null) {
            ContextComponentTuple tuple = getOrCreateContextComponentTuple(contextId);
            tuple.getContext().addSecurityHandler(securityHandler);
        }
    }

    public void removeSecurityHandler(ServiceReference serviceReference, Object service) {
        SecurityHandler securityHandler = (SecurityHandler) service;
        String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId != null) {
            ContextComponentTuple tuple = m_httpContextComponents.get(contextId);
            if (tuple != null) {
                tuple.getContext().removeSecurityHandler(securityHandler);
            }
        }
    }

    public void addMimetypeResolver(ServiceReference serviceReference, Object service) {
        MimeTypeResolver mimeTypeResolver = (MimeTypeResolver) service;
        String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId != null) {
            ContextComponentTuple tuple = getOrCreateContextComponentTuple(contextId);
            tuple.getContext().addMimeTypeResolver(mimeTypeResolver);
        }
    }

    public void removeMimetypeResolver(ServiceReference serviceReference, Object service) {
        MimeTypeResolver mimeTypeResolver = (MimeTypeResolver) service;
        String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId != null) {
            ContextComponentTuple tuple = m_httpContextComponents.get(contextId);
            if (tuple != null) {
                tuple.getContext().removeMimeTypeResolver(mimeTypeResolver);
            }
        }
    }

    private String getStringProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof String) ? (String) value : null;
    }

    private ContextComponentTuple getOrCreateContextComponentTuple(String contextId) {
        ContextComponentTuple tuple = m_httpContextComponents.get(contextId);
        if (tuple == null) {

            DelegatingHttpContext context = new DelegatingHttpContext(contextId);

            Dictionary<String, Object> properties = new Hashtable<String, Object>();
            properties.put(CONTEXT_ID_KEY, contextId);

            Component component = m_dependencyManager
                .createComponent()
                .setInterface(HttpContext.class.getName(), properties)
                .setImplementation(context);

            tuple = new ContextComponentTuple(context, component);

            if (m_httpContextComponents.putIfAbsent(contextId, tuple) == null) {
                m_dependencyManager.add(component);
            }
        }
        return tuple;
    }

    static class ContextComponentTuple {

        private final DelegatingHttpContext m_context;
        private final Component m_component;

        public ContextComponentTuple(DelegatingHttpContext context, Component component) {
            m_context = context;
            m_component = component;
        }

        public DelegatingHttpContext getContext() {
            return m_context;
        }

        public Component getComponent() {
            return m_component;
        }
    }
}
