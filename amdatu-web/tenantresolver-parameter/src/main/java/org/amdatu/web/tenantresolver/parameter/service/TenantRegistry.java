/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.tenantresolver.parameter.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.tenant.Tenant;

/**
 * {@code TenantRegistry} keeps a map of available {@link Tenant} services using their
 * PID. This provides faster lookups and reduces contention on the service registry.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class TenantRegistry {

    private final Map<String, Tenant> m_pidToTenant = new ConcurrentHashMap<String, Tenant>();

    public Tenant getTenant(String tenantPID) {
        return m_pidToTenant.get(tenantPID);
    }

    public void removeTenant(Tenant tenant) {
        m_pidToTenant.remove(tenant.getPID());
    }

    public void addTenant(Tenant tenant) {
        m_pidToTenant.put(tenant.getPID(), tenant);
    }
}