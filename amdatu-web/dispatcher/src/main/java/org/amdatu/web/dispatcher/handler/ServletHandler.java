/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class ServletHandler extends AbstractHandler implements Comparable<ServletHandler> {

    private final String m_alias;
    private final Servlet m_servlet;

    public ServletHandler(int handlerId, int ranking, String contextId, Servlet servlet,
        String alias, String tenantPID) {
        super(handlerId, ranking, contextId, tenantPID);
        if (alias == null || alias.equals("")) {
            m_alias = "/";
        }
        else {
            m_alias = alias;
        }
        m_servlet = servlet;
    }

    public String getAlias() {
        return m_alias;
    }

    public Servlet getServlet() {
        return m_servlet;
    }

    public void doInit() throws ServletException {
        String name = "servlet_" + getId();
        ServletConfig config = new ServletConfigImpl(name, getContext(), getInitParams());
        m_servlet.init(config);
    }

    public void doDestroy() {
        m_servlet.destroy();
    }

    public boolean handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
        throws ServletException, IOException {
        if (isActive()) {
            if (!getContext().handleSecurity(httpServletRequest, httpServletResponse)) {
                if (!httpServletResponse.isCommitted()) {
                    httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
                }
            }
            else {
                m_servlet.service(httpServletRequest, httpServletResponse);
            }
            return true;
        }
        return false;
    }

    public boolean matches(String path) {
        if (path == null) {
            return m_alias.equals("/");
        }
        else if (m_alias.equals("/")) {
            return path.startsWith(m_alias);
        }
        else {
            return path.equals(m_alias) || path.startsWith(m_alias + "/");
        }
    }

    public int compareTo(ServletHandler other) {
        if (other.m_alias.length() == m_alias.length()) {
            if (m_ranking == other.m_ranking) {
                return m_handlerId - other.m_handlerId;
            }
            else {
                return other.m_ranking - m_ranking;
            }
        }
        return other.m_alias.length() - m_alias.length();
    }
}
