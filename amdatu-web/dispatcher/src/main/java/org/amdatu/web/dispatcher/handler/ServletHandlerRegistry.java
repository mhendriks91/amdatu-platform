/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import static org.amdatu.tenant.Constants.PID_KEY;
import static org.osgi.framework.Constants.SERVICE_ID;
import static org.osgi.framework.Constants.SERVICE_RANKING;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.amdatu.web.dispatcher.Constants;
import org.amdatu.web.dispatcher.context.HandlerServletContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpContext;
import org.osgi.service.log.LogService;

/**
 * Handler registry that manages <code>FilterHandler</code> instances lifecycle
 * based on <code>HttpContext</code> availability.
 * 
 * TODO revisit threadsafety / spawning threads
 * TODO consider caching for getServletHandler
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class ServletHandlerRegistry extends AbstractHandlerRegistry {
    private static final String MAGIC_NOTENANT_PID = "";
    private final ReentrantReadWriteLock m_servletHandlersLock = new ReentrantReadWriteLock();
    private final Map<ServiceReference, ServletHandler> m_servletHandlers =
        new HashMap<ServiceReference, ServletHandler>();
    private final Map<String, ServletHandler[]> m_tenantServletHandlerArrays = new HashMap<String, ServletHandler[]>();

    @Override
    public void httpContextAdded(String contextId, HttpContext context) {

        m_servletHandlersLock.readLock().lock();
        try {

            for (ServletHandler handler : m_servletHandlers.values()) {

                if (!handler.isActive() && handler.getContextId().equals(contextId)) {
                    final ServletHandler finalHandler = handler;
                    HandlerServletContext servletContextWrapper =
                        new HandlerServletContext(getServletContext(), context);
                    finalHandler.setExtServletContext(servletContextWrapper);

                    // outside lock
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                finalHandler.init();
                            }
                            catch (ServletException e) {
                                finalHandler.destroy();
                            }
                        }
                    }).start();
                }
            }
        }
        finally {
            m_servletHandlersLock.readLock().unlock();
        }
    }

    @Override
    public void httpContextRemoved(String contextId) {

        m_servletHandlersLock.readLock().lock();
        try {

            for (ServletHandler handler : m_servletHandlers.values()) {
                final ServletHandler finalHandler = handler;
                if (finalHandler.isActive() && finalHandler.getContextId().equals(contextId)) {

                    // outside lock
                    new Thread(new Runnable() {
                        public void run() {
                            finalHandler.destroy();
                        }
                    }).start();
                }
            }
        }
        finally {
            m_servletHandlersLock.readLock().unlock();
        }
    }

    public void addServletHandler(ServiceReference serviceReference, Servlet servlet) {

        int serviceId = getIntProperty(serviceReference, SERVICE_ID, 0);
        int serviceRanking = getIntProperty(serviceReference, SERVICE_RANKING, 0);
        String contextId = getStringProperty(serviceReference, Constants.CONTEXT_ID_KEY);
        if (contextId == null) {
            contextId = "";
        }
        String alias = getStringProperty(serviceReference, Constants.ALIAS_KEY);
        if (alias == null) {
            if (getLogService() != null) {
                getLogService().log(LogService.LOG_WARNING,
                    "Cannot register a servlet without alias (" + SERVICE_ID + "=" + serviceId + ")");
            }
            return;
        }

        String tenant = getStringProperty(serviceReference, PID_KEY);
        final ServletHandler handler =
            new ServletHandler(serviceId, serviceRanking, contextId, servlet, alias, tenant);
        handler.setInitParams(getInitParams(serviceReference));

        m_servletHandlersLock.writeLock().lock();
        try {
            if (m_servletHandlers.containsKey(serviceReference)) {
                throw new IllegalStateException("Unexpected.... ");
            }

            HttpContext context = getHttpContext(contextId, serviceReference.getBundle());
            if (context != null) {
                HandlerServletContext servletContextWrapper =
                    new HandlerServletContext(getServletContext(), context);
                handler.setExtServletContext(servletContextWrapper);

                // outside lock
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            handler.init();
                        }
                        catch (ServletException e) {
                            handler.destroy();
                        }
                    }
                }).start();
            }

            m_servletHandlers.put(serviceReference, handler);
            m_tenantServletHandlerArrays.clear();
        }
        finally {
            m_servletHandlersLock.writeLock().unlock();
        }
    }

    public void removeServletHandler(ServiceReference serviceReference, Servlet servlet) {

        m_servletHandlersLock.writeLock().lock();
        try {
            if (!m_servletHandlers.containsKey(serviceReference)) {
                return;
            }
            final ServletHandler servletHandler = m_servletHandlers.remove(serviceReference);
            m_tenantServletHandlerArrays.clear();
            if (servletHandler != null) {

                // outside lock
                new Thread(new Runnable() {
                    public void run() {
                        servletHandler.destroy();
                    }
                }).start();

                servletHandler.destroy();
            }
        }
        finally {
            m_servletHandlersLock.writeLock().unlock();
        }
    }

    public ServletHandler getServletHandler(HttpServletRequest httpServletRequest, String path) {

        if (path == null) {
            return null;
        }
        if (path.equals("")) {
            path = "/";
        }

        ServletHandler[] servletHandlers = getServletHandlers(httpServletRequest);
        if (servletHandlers == null) {
            return null;
        }

        ServletHandler target = null;
        for (ServletHandler handler : servletHandlers) {
            if (handler.isActive() && handler.matches(path)) {
                target = handler;
                break;
            }
        }
        return target;
    }

    public ServletHandler getServletHandler(HttpServletRequest httpServletRequest) {

        String localRequestUri = httpServletRequest.getRequestURI();
        if (!httpServletRequest.getContextPath().equals("")) {
            localRequestUri = localRequestUri.replaceFirst(httpServletRequest.getContextPath(), "");
        }

        return getServletHandler(httpServletRequest, localRequestUri);
    }

    public ServletHandler[] getServletHandlers(HttpServletRequest httpServletRequest) {
        String tenantPID = (String) httpServletRequest.getAttribute(Constants.TENANTPID_REQUESTCONTEXT_KEY);
        if (tenantPID == null) {
            tenantPID = MAGIC_NOTENANT_PID;
        }

        m_servletHandlersLock.readLock().lock();
        try {
            ServletHandler[] handlerArray = m_tenantServletHandlerArrays.get(tenantPID);
            if (handlerArray != null) {
                return handlerArray;
            }
        }
        finally {
            m_servletHandlersLock.readLock().unlock();
        }

        m_servletHandlersLock.writeLock().lock();
        try {
            // retry
            ServletHandler[] handlerArray = m_tenantServletHandlerArrays.get(tenantPID);
            if (handlerArray != null) {
                return handlerArray;
            }

            // build
            if (tenantPID.equals(MAGIC_NOTENANT_PID)) {
                List<ServletHandler> filterHandlers = new LinkedList<ServletHandler>();
                for (ServletHandler filterHandler : m_servletHandlers.values()) {
                    if (filterHandler.getTenantPID() == null || filterHandler.getTenantPID() == MAGIC_NOTENANT_PID) {
                        filterHandlers.add(filterHandler);
                    }
                }
                handlerArray = filterHandlers.toArray(new ServletHandler[filterHandlers.size()]);
            }
            else {
                List<ServletHandler> filterHandlers = new LinkedList<ServletHandler>();
                for (ServletHandler servletHandler : m_servletHandlers.values()) {
                    if (servletHandler.getTenantPID() == null || servletHandler.getTenantPID() == MAGIC_NOTENANT_PID
                        || servletHandler.getTenantPID().equals(tenantPID)) {
                        filterHandlers.add(servletHandler);
                    }
                }
                handlerArray = filterHandlers.toArray(new ServletHandler[filterHandlers.size()]);
            }
            Arrays.sort(handlerArray);
            m_tenantServletHandlerArrays.put(tenantPID, handlerArray);
            return handlerArray;
        }
        finally {
            m_servletHandlersLock.writeLock().unlock();
        }
    }
}
