/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.context;

import org.osgi.service.http.HttpContext;
import org.osgi.framework.Bundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;

/**
 * Temporary default implementation of <code>HttpContext</code>.
 * TODO transfer responsibility to httpcontext (AMDATU-283)
 */
public final class DefaultHttpContext implements HttpContext {

    private final Bundle m_bundle;

    public DefaultHttpContext(Bundle bundle) {
        m_bundle = bundle;
    }

    public String getMimeType(String name) {
        return null;
    }

    public URL getResource(String name) {
        if (name.startsWith("/")) {
            name = name.substring(1);
        }
        return m_bundle.getResource(name);
    }

    public boolean handleSecurity(HttpServletRequest req, HttpServletResponse res) {
        return true;
    }
}
