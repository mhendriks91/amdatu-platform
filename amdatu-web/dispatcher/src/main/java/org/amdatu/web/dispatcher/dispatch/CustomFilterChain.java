/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.dispatch;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.dispatcher.handler.FilterHandler;
import org.amdatu.web.dispatcher.handler.ServletHandler;

/**
 * Custom <code>FilterChain</code> implementation that invokes an array of <code>FilterHandler</code>
 * instances wrapping <code>Filter</code> objects and subsequently invokes the specified <code>
 * CustomServletPipeline</code>. If a servlet in the <code>CustomServletPipeline</code> handles
 * the request we are done. If not we return to the specified proceeding chain.
 */
public final class CustomFilterChain extends HttpFilterChain {

    private final FilterHandler[] m_filterHandlers;
    private final ServletHandler m_targetServletHandler;
    private final FilterChain m_proceedingChain;
    private int m_index = -1;

    public CustomFilterChain(FilterHandler[] filterHandlers, ServletHandler targetServletHandler,
        FilterChain proceedingChain) {
        m_filterHandlers = filterHandlers;
        m_targetServletHandler = targetServletHandler;
        m_proceedingChain = proceedingChain;
    }

    protected void doFilter(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException {
        this.m_index++;

        if (m_index < m_filterHandlers.length) {
            m_filterHandlers[m_index].handle(req, res, this);
        }
        else {
            if (!m_targetServletHandler.handle(req, res)) {
                m_proceedingChain.doFilter(req, res);
            }
        }
    }
}
