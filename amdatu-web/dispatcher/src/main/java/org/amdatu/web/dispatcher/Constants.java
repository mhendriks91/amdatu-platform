/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher;

/**
 * Provides some common constants used throughout the various web projects.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface Constants {

    /**
     * <code>HttpServletRequest</code> attribute key where tenant resolving extenders
     * must store the <code>Tenant</code> PID.
     */
    String TENANTPID_REQUESTCONTEXT_KEY = "org.amdatu.web.dispatcher.TENANTPID";

    /**
     * <code>HttpServletRequest</code> attribute key where tenant resolving extenders
     * must store the <code>Tenant</code> instance.
     */
    String TENANT_REQUESTCONTEXT_KEY = "org.amdatu.web.dispatcher.TENANT";

    /**
     * <code>Servlet</code> and <code>Filter</code> service registration property
     * considered for reusing or creating a specific named <code>HttpContext</code>.
     */
    String CONTEXT_ID_KEY = "contextId";

    /**
     * <code>Filter</code> service registration property indicating for which requested
     * paths it should be invoked using a regular expression format. Filters are
     * ordered based on service ranking.
     */
    String PATTERN_KEY = "pattern";

    /**
     * <code>Servlet</code> service registration property indicating for which requested
     * paths this servlet should be considered. Servlets are matched according to most
     * specific path followed by standard service ranking.
     */
    String ALIAS_KEY = "alias";
}
