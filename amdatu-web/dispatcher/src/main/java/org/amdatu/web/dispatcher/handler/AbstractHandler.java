/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.amdatu.web.dispatcher.context.ExtServletContext;

/**
 * Abstract handler implementing base logic for <code>ServletHandler</code> and
 * <code>FilterHandler</code>.
 */
public abstract class AbstractHandler {

    public enum State {
        NEW,
            INITIALIZED,
            DESTROYED
    }

    private final Map<String, String> m_initParams = new HashMap<String, String>();
    protected final String m_tenantPID;
    protected final int m_handlerId;
    protected final int m_ranking;

    protected final String m_contextId;

    private ExtServletContext m_servletContext;
    private volatile State m_state;

    public AbstractHandler(int handlerId, int ranking, String tenantPID) {
        this(handlerId, ranking, "", tenantPID);
    }

    public AbstractHandler(int handlerId, int ranking, String contextId, String tenantPID) {
        m_handlerId = handlerId;
        m_ranking = ranking;
        m_contextId = contextId;
        m_tenantPID = tenantPID;
        m_state = State.NEW;
    }

    public final int getId() {
        return m_handlerId;
    }

    public final String getContextId() {
        return m_contextId;
    }

    public final int getRanking() {
        return m_ranking;
    }

    public final String getTenantPID() {
        return m_tenantPID;
    }

    public final State getState() {
        return m_state;
    }

    public final boolean isActive() {
        return m_state == State.INITIALIZED;
    }

    protected final ExtServletContext getContext() {
        return m_servletContext;
    }

    public final void setExtServletContext(ExtServletContext context) {
        m_servletContext = context;
    }

    public final Map<String, String> getInitParams() {
        return m_initParams;
    }

    public final void setInitParams(Dictionary<String, String> map) {
        m_initParams.clear();
        if (map == null) {
            return;
        }
        Enumeration<String> e = map.keys();
        while (e.hasMoreElements()) {
            String key = e.nextElement();
            m_initParams.put(key, map.get(key));
        }
    }

    public final void init() throws ServletException {
        doInit();
        m_state = State.INITIALIZED;
    }

    public final void destroy() {
        if (m_state == State.INITIALIZED) {
            doDestroy();
        }
        m_state = State.DESTROYED;
    }

    protected abstract void doInit() throws ServletException;

    protected abstract void doDestroy();
}
