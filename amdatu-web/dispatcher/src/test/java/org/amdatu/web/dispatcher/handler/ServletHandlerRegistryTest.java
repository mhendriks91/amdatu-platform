/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import javax.servlet.ServletContext;

import junit.framework.Assert;

import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;
import org.osgi.service.log.LogService;

public class ServletHandlerRegistryTest {

    private Mockery m_context;

    @Before
    public void setUp() {
        m_context = new Mockery();
    }

    @Test
    public void testFilterReg() {
        LogService logService = m_context.mock(LogService.class);
        ServletContext servletContext = m_context.mock(ServletContext.class);

        ServletHandlerRegistry sr = new ServletHandlerRegistry();
        sr.setLogService(logService);
        sr.setServletContext(servletContext);

        // FIXME to be done
        Assert.assertTrue(true);
    }
}
