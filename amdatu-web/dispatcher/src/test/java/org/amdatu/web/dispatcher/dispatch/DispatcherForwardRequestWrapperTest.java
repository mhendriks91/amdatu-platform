/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.dispatch;

import javax.servlet.http.HttpServletRequest;

import junit.framework.Assert;

import org.amdatu.web.dispatcher.dispatch.CustomFilterPipeline.DispatcherForwardRequestWrapper;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

public class DispatcherForwardRequestWrapperTest {

    @Test
    public void testForwardWrapping() {

        final Mockery mockContext = new Mockery();
        final HttpServletRequest request = mockContext.mock(HttpServletRequest.class);
        mockContext.checking(new Expectations() {
            {
                allowing(request).getRequestURL();
                will(returnValue(new StringBuffer(
                    "http://localhost:3737/amdatu/dashboard/jsp/dashboard.jsp?hello=world")));
                allowing(request).getContextPath();
                will(returnValue("/amdatu"));
                allowing(request).getRequestURI();
                will(returnValue("/amdatu/dashboard/jsp/dashboard.jsp"));
                allowing(request).getServletPath();
                will(returnValue("/dashboard/jsp"));
                allowing(request).getPathInfo();
                will(returnValue("/dashboard.jsp"));
                allowing(request).getQueryString();
                will(returnValue("hello=world"));
            }
        });

        DispatcherForwardRequestWrapper wrapper =
            new DispatcherForwardRequestWrapper(request, "/whatever/alias", "/whatever/alias/bla.jsp");

        Assert
            .assertEquals("http://localhost:3737/amdatu/whatever/alias/bla.jsp?hello=world",
                wrapper.getRequestURL().toString());
        Assert.assertEquals("/amdatu", wrapper.getContextPath());
        Assert.assertEquals("/amdatu/whatever/alias/bla.jsp", wrapper.getRequestURI());
        Assert.assertEquals("/whatever/alias", wrapper.getServletPath());
        Assert.assertEquals("/bla.jsp", wrapper.getPathInfo());
        Assert.assertEquals("hello=world", wrapper.getQueryString());
    }
}
