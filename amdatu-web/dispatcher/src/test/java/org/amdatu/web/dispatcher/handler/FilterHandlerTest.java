/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import java.util.Arrays;

import javax.servlet.Filter;

import junit.framework.Assert;

import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

public class FilterHandlerTest {

    private Mockery m_context;

    @Before
    public void setUp() {
        m_context = new Mockery();
    }

    @Test
    public void testFilterHandlerCreate() {

//      ExtServletContext context = m_context.mock(ExtServletContext.class);
        String contextId = "123";
        Filter filter = m_context.mock(Filter.class);

        FilterHandler fh1 = new FilterHandler(1, contextId, filter, "aaa", 2, "t1");
        Assert.assertEquals(1, fh1.getId());
        Assert.assertEquals(fh1.getTenantPID(), "t1");
        Assert.assertEquals(fh1.getContextId(), contextId);
        Assert.assertEquals(fh1.getRanking(), 2);
        Assert.assertEquals(fh1.getPattern(), "aaa");
        Assert.assertEquals(fh1.getFilter(), filter);
    }

    @Test
    public void testFilterHandlerMatching() {

//      ExtServletContext context = m_context.mock(ExtServletContext.class);
        String contextId = "123";
        Filter filter = m_context.mock(Filter.class);

        FilterHandler fh1 = new FilterHandler(1, contextId, filter, ".*", 0, "");
        Assert.assertTrue(fh1.matches(null));
        Assert.assertTrue(fh1.matches(""));
        Assert.assertTrue(fh1.matches("/"));
        Assert.assertTrue(fh1.matches("/whatever"));

        FilterHandler fh2 = new FilterHandler(2, contextId, filter, "/.*", 2, "");
        Assert.assertTrue(fh2.matches(null));
        Assert.assertTrue(fh2.matches("/"));
        Assert.assertTrue(fh2.matches("/whatever"));

        FilterHandler fh3 = new FilterHandler(3, contextId, filter, "/hello.*", 2, "");
        Assert.assertFalse(fh3.matches(null));
        Assert.assertFalse(fh3.matches("/"));
        Assert.assertTrue(fh3.matches("/hello"));
        Assert.assertTrue(fh3.matches("/hellowww"));
        Assert.assertTrue(fh3.matches("/hello/"));
        Assert.assertTrue(fh3.matches("/hello/world"));
        Assert.assertFalse(fh3.matches("/hell0"));
        Assert.assertFalse(fh3.matches("/hell0/"));
        Assert.assertFalse(fh3.matches("/hell0/world"));
    }

    @Test
    public void testFilterHandlerSorting() {

//      ExtServletContext context = m_context.mock(ExtServletContext.class);
        String contextId = "123";
        Filter filter = m_context.mock(Filter.class);

        FilterHandler fh1 = new FilterHandler(1, contextId, filter, ".*", 0, "");
        FilterHandler fh2 = new FilterHandler(2, contextId, filter, ".*", 2, "");
        FilterHandler fh3 = new FilterHandler(3, contextId, filter, ".*", 1, "");
        FilterHandler fh4 = new FilterHandler(4, contextId, filter, ".*", 1, "");

        // higher service.ranking rules
        // lower service.id rules when service.ranking equals
        FilterHandler[] expectedArray = new FilterHandler[] { fh2, fh3, fh4, fh1 };
        FilterHandler[] sortedArray = new FilterHandler[] { fh1, fh2, fh3, fh4 };
        Arrays.sort(sortedArray);
        Assert.assertTrue(Arrays.equals(expectedArray, sortedArray));
    }
}
