/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.libraries.osgi;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

/**
 * Helper class to create an activator that needs other services to be 'up' before the rest
 * of the activator is executed.
 * The most prominent use case for this activator is the use of 'marker' interface that state
 * the availability of some non-OSGi service.
 */
public abstract class ServiceDependentActivator extends DependencyActivatorBase {
    @Override
    public final void init(BundleContext context, DependencyManager manager) throws Exception {
        Object watcher = new SpiWatcher();

        Component spiWatcherComponent = createComponent().setImplementation(watcher);
        for (Class<?> spiService : getRequiredServices()) {
            spiWatcherComponent.add(createServiceDependency().setService(spiService).setRequired(true));
        }

        manager.add(spiWatcherComponent);
    }

    /**
     * Gets an array of all services that should be 'up' before the rest of activator is started.
     * Once the rest of the activator is started, you will not have access to these services by default.
     */
    protected abstract Class<?>[] getRequiredServices();

    public abstract void initWithDependencies(BundleContext context, DependencyManager manager) throws Exception;

   private class SpiWatcher {
        private volatile DependencyManager m_dependencyManager;
        private volatile BundleContext m_bundleContext;
        private AtomicBoolean m_started = new AtomicBoolean(false);

        @SuppressWarnings("unused")
        public void start() throws Exception {
            if (m_started.compareAndSet(false, true)) {
                initWithDependencies(m_bundleContext, m_dependencyManager);
            }
        }
    }
}
