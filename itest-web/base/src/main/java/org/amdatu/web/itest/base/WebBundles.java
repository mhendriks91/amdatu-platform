/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.base;

import static org.ops4j.pax.exam.CoreOptions.composite;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.systemPackages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.amdatu.itest.base.Fixture;
import org.amdatu.itest.base.ProvisionedBundle;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.options.ProvisionOption;
import org.ops4j.pax.exam.options.UrlProvisionOption;

/**
 * Provides a mean to provision all core web-bundles.
 */
public enum WebBundles implements ProvisionedBundle {

    SERVLET("javax.servlet", "servlet-api", false),
        ITEST_BASE("org.amdatu.itest", "org.amdatu.itest.web.base", true),
        JETTY("org.amdatu.httpservice", "org.amdatu.multitenant.org.apache.felix.http.jetty", false),
        DISPATCHER("org.amdatu.web", "org.amdatu.web.dispatcher", true),
        HTTPCONTEXT("org.amdatu.web", "org.amdatu.web.httpcontext", true),
        JSP("org.amdatu.web", "org.amdatu.web.jsp", true),
        RESOURCE("org.amdatu.web", "org.amdatu.web.resource", true),
        JAXRS("org.amdatu.web", "org.amdatu.web.jaxrs", true),
        WINK("org.amdatu.web", "org.amdatu.web.wink", true),
        TENANTRESOLVER_PARAMETER("org.amdatu.web", "org.amdatu.web.tenantresolver.parameter", true),
        TENANTRESOLVER_HOSTNAME("org.amdatu.web", "org.amdatu.web.tenantresolver.hostname", true);

    private final String m_groupId;
    private final String m_artifactId;
    private final boolean m_addCoverageClassifier;

    private WebBundles(String groupId, String artifactId, boolean addCoverageClassier) {
        m_groupId = groupId;
        m_artifactId = artifactId;
        m_addCoverageClassifier = addCoverageClassier;
    }

    /**
     * Convenience method to provision all of the core bundles.
     *
     * @return a PAX-exam provisioning option with all core bundles.
     */
    public static Option provisionAll() {
        return provisionAllExcluding();
    }

    /**
     * Convenience method to provision a subset of the core bundles.
     *
     * @param excludes the optional core bundles to exclude from provisioning.
     * @return a PAX-exam provisioning option with the requested subset of core bundles.
     */
    public static Option provisionAllExcluding(WebBundles... excludes) {
        List<WebBundles> values = new ArrayList<WebBundles>(Arrays.asList(values()));
        values.removeAll(Arrays.asList(excludes));
        return composite(
            systemPackages("org.w3c.dom.traversal"),
            Fixture.provision(values.toArray(new WebBundles[values.size()])));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.amdatu.itest.base.ProvisionedBundle#getProvisionOption()
     */
    public ProvisionOption<?> getProvisionOption() {
        ProvisionOption<?> option =
            mavenBundle().groupId(m_groupId).artifactId(m_artifactId).versionAsInProject().start(false);
        if (m_addCoverageClassifier) {
            option = new UrlProvisionOption(option.getURL() + "//" + Fixture.getCoverageClassifier());
        }
        return option;
    }
}