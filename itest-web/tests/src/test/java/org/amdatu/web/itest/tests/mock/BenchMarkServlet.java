/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.tests.mock;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Simple integration test servlet that counts invocations.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class BenchMarkServlet extends HttpServlet {

    private static final long serialVersionUID = 4739679663733054394L;

    private final AtomicInteger m_doGetCount = new AtomicInteger();

    private final String m_returnMessage;
    private final int m_contentLength;

    public BenchMarkServlet(String returnMessage) {
        m_returnMessage = returnMessage;
        m_contentLength = m_returnMessage == null ? 0 : m_returnMessage.length();
    }

    public int getDoGetCount() {
        return m_doGetCount.intValue();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("wait") == null) {
            m_doGetCount.incrementAndGet();
        }

        resp.setContentType("text/plain");
        resp.setContentLength(m_contentLength);

        PrintWriter pw = resp.getWriter();
        pw.append(m_returnMessage);
        pw.close();
    }
}
