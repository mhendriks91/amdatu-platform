Amdatu Platform Readme 
======================

This document is is a general info/intro file for developers that have checked out
the Amdatu code repository. For more and/or detailed info on the Amdatu project
please refer to the project websites.

 * WIKI -> http://www.amdatu.org
 * JIRA -> http://jira.amdatu.org


Repository
==========

The Amdatu Platform repository uses Git and is hosted on Bitbucket. You can find
it here: https://bitbucket.org/amdatu/amdatu-platform
   
Individual subprojects will, as soon as they migrate, get their own repository
here: https://bitbucket.org/amdatu

Building
========

The Amdatu build infrastructure is based on Maven 2. The default goal builds,
tests, packages and installs all artifacts. Modules can also be build
individually by stepping into the directory. In addition several profiles
exist to speed up builds during development.

e.g.

    mvn                  # build everything
    mvn -P!itest         # skip integration tests
    mvn -P!release       # skip javadoc and more

    mvn package \        # fastest build
    -P!itest,!release \
    -Dmaven.test.skip=true
