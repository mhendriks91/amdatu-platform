@echo off

rem Open a debug port
set JAVA_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n

rem Set memory options
set JAVA_OPTS=%JAVA_OPTS% -Xms256m -Xmx1024m -XX:MaxPermSize=256m

rem Felix property file
set JAVA_OPTS=%JAVA_OPTS% -Dfelix.config.properties=file:conf/amdatu-platform.properties

rem Set encoding to UTF-8
set JAVA_OPTS=%JAVA_OPTS% -Dfile.encoding=utf-8

echo Starting Amdatu server
echo JAVA_OPTS=%JAVA_OPTS%

java %JAVA_OPTS% -jar lib/org.apache.felix.main-${org.apache.felix.main.version}.jar

